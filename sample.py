import pylab
import os
import numpy as np

x = np.random.uniform(0, 1, 50)
y = np.random.uniform(0, 1, 50)
pylab.plot(x, y)

pylab.savefig('sample.png')
path = os.path.realpath("sample.png")
print("plot?" + path)
