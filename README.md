# Brain Injury Research System

This README contains updates and feature information for each released test version.


##

### Version V0.1
* Release Date: ___2016-02-22___
* Release Commit: ___406d4b6: merge Eddie's Update on instruction___
* Main Updates: 

 1. Xiaohui's instruction update with larger font size and image instruction for Distance Vergence;


### Version V0.2
* Release Date: ___2016-03-24___
* Release Commit: ___43bfe1c: Merge the updates on the tablet___
* Main Updates: 
 
 1.  Eric's instruction update with larger font size;
 2.  Xiaohui's Update on the image path;
 3.  Updated the segmentation of the Distance Vergence.

### Version V0.4
* Release Date: ___2016-09-21___
* Release Commit: ___fcbf974: GUI Updates___
* Main Updates:

1. Removed extra fields in first patient information panel
2. Changed order of target presentation for Saccade and Anti-Saccade instructions