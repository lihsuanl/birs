/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PatientManagement;

import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;

enum QuestionType {
    TEXT, RADIO, CHECKLIST, CHECKLIST_WITH_TEXT
}

/**
 *
 * @author Eric Dong
 */
public class PatientManagementQuestionJPanel extends JPanel {
    //  Class members
    private QuestionType qType;
    private String question;
    private String subtext;     //  Optional
    
    private ArrayList<SurveyOption> options;
 
    
    public class SurveyOption {
        public PatientManagementQuestionJPanel parent;
        public JComponent optionComponent;  //  JTextField, JCheckBox, or JRadioButton
        public String label;
    }
}