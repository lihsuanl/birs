/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AntiSaccadeTest;

import Business.Business.Business;
import Business.DataWriter.DataWriter;
import Business.Patient.Patient;
import Business.Task.AntiSaccadeTask;
import UserInterface.TestResult.TestResultJPanel;
import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.IGazeListener;
import com.theeyetribe.client.data.GazeData;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author neethan
 */
public class AntiSaccadeTestJPanel extends javax.swing.JPanel implements ActionListener, IGazeListener {

    /**
     * Creates new form AntiSaccadeTestJPanel
     */
    private JPanel userProcessContainer;
    private Patient patient;
    private Business business;
    private AntiSaccadeTask antisaccadeTask;
    private GazeManager gm;
    private BufferedImage image;
    private DataWriter dataWriter;
    private Date testStart;

    private int xCenter;
    private int yCenter;
    private double targetOffset, pulses, pulseWidthMax, pulseWidth, screenDistanceInches;

    private int diameter;
    private int smallDiameter;
    private double xLocR, xLocL, xLocC;
    private double xLoc, yLoc;
    private int xLocCenterDraw, xLocRightDraw, xLocLeftDraw, xLocCenterDrawSmall, xLocRightDrawSmall, xLocLeftDrawSmall, yLocDraw, yLocDrawSmall;
    private int screenResolution = Toolkit.getDefaultToolkit().getScreenResolution();

    private int pulseResolution;
    private int currentResolution;
    private Random random;
    private Timer pulseTimer;
    private Timer testTimer;
    private java.util.Timer instructionTimer;
    private double focusTimeLow, focusTimeHigh, presentationsPerSide, pulseTime;
    private boolean leftPulsingFreeze, rightPulsingFreeze, centerPulsingFreeze;
    private int leftToShow, rightToShow;
    private double pulseAngle;
    private boolean leftPulsing;
    private boolean rightPulsing;
    private XYSeries targetLine = new XYSeries("Target");
    private XYSeries gazeLine = new XYSeries("Gaze");
    private XYSeriesCollection lineDataSet = new XYSeriesCollection();
    private ArrayList<XYSeriesCollection> lineDataSets = new ArrayList<XYSeriesCollection>();
    private int errorTimes = 0;
    private int recordedTimes = 0;
    private final Object lock = new Object();
    private boolean instruction = true;

    public AntiSaccadeTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.patient = patient;
        this.gm = gm;
        antisaccadeTask = new AntiSaccadeTask();
        instructionTimer = new java.util.Timer();

        //  Hide the things
        startJBtn.setVisible(false);
        stopJBtn.setVisible(false);
        lookJLabel.setVisible(false);
        lookJLabel1.setVisible(false);
        lookJLabel2.setVisible(false);
        leftPulsingFreeze = false;
        rightPulsingFreeze = false;
        centerPulsingFreeze = false;

        instructionsPhaseOne(false);
    }

    private void initiateAnimationParams() {
        xCenter = this.getPreferredSize().width / 2;
        yCenter = this.getPreferredSize().height / 2;
        pulseResolution = 20;
        diameter = 25;
        pulseAngle = 0;
        smallDiameter = 10;
        focusTimeLow = (Double) antisaccadeTask.getTaskParams().get("Focus Time Low");
        focusTimeHigh = (Double) antisaccadeTask.getTaskParams().get("Focus Time High");
        presentationsPerSide = (Double) antisaccadeTask.getTaskParams().get("Presentation Per Side");
        pulseTime = (Double) antisaccadeTask.getTaskParams().get("Pulse Time");
        pulses = (Double) antisaccadeTask.getTaskParams().get("Pulses");
        pulseWidthMax = (Double) antisaccadeTask.getTaskParams().get("Pulse Width Max");
        screenDistanceInches = (Double) antisaccadeTask.getTaskParams().get("Screen Distance (In)");
        xLoc = xCenter;
        yLoc = yCenter;
        xLocC = xCenter;
        targetOffset = this.getPreferredSize().width * 3 / 7;
        xLocL = xLocC - targetOffset;
        xLocR = xLocC + targetOffset;
        xLocCenterDraw = (int) (xLocC - diameter / 2);
        xLocRightDraw = (int) (xLocR - diameter / 2);
        xLocLeftDraw = (int) (xLocL - diameter / 2);
        xLocCenterDrawSmall = (int) (xLocC - smallDiameter / 2);
        xLocRightDrawSmall = (int) (xLocR - smallDiameter / 2);
        xLocLeftDrawSmall = (int) (xLocL - smallDiameter / 2);
        yLocDraw = (int) (yCenter - diameter / 2);
        yLocDrawSmall = (int) (yCenter - smallDiameter / 2);
        random = new Random();
        pulseTimer = new Timer(pulseResolution, new PulseTimerAction());
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //if (testTimer.isRunning()) {
        if (!leftPulsing && !rightPulsing) {
            g.fillOval(xLocCenterDraw, yLocDraw, diameter, diameter);
        }

        //Paint Left Object
        if (leftPulsing) {
            g.fillOval((int) (xLocLeftDraw - pulseWidth / 2), (int) (yLocDraw - pulseWidth / 2), (int) (diameter + pulseWidth), (int) (diameter + pulseWidth));
        } else {
            g.fillOval(xLocLeftDraw, yLocDraw, diameter, diameter);
            g.fillOval(xLocCenterDraw, yLocDraw, diameter, diameter);
        }
        //Paint Right Object
        if (rightPulsing) {
            g.fillOval((int) (xLocRightDraw - pulseWidth / 2), (int) (yLocDraw - pulseWidth / 2), (int) (diameter + pulseWidth), (int) (diameter + pulseWidth));
        } else {
            g.fillOval(xLocRightDraw, yLocDraw, diameter, diameter);
            g.fillOval(xLocCenterDraw, yLocDraw, diameter, diameter);
        }
        g.setColor(Color.white);
        g.fillOval(xLocLeftDrawSmall, yLocDrawSmall, smallDiameter, smallDiameter);
        g.fillOval(xLocRightDrawSmall, yLocDrawSmall, smallDiameter, smallDiameter);
        g.fillOval(xLocCenterDrawSmall, yLocDrawSmall, smallDiameter, smallDiameter);
        //}

    }

    public int randInt(int min, int max) {
        int randomNum = random.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    private void startTestTimer(int resolution) {
        if (testTimer != null) {
            testTimer.stop();
            testTimer.setDelay(resolution);
        } else {
            testTimer = new Timer(resolution, this);
        }

        testTimer.start();
        testTimer.setRepeats(false);
        antisaccadeTask.setIsDone(false);
    }

    private void testFinished() {
        antisaccadeTask.setResultTitle("Precentage Recorded: " + (double) recordedTimes / (recordedTimes + errorTimes));
        TestResultJPanel trjp = new TestResultJPanel(userProcessContainer, business, patient, gm, antisaccadeTask, lineDataSets);
        userProcessContainer.add("Anti Saccade Test Result JPanel", trjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleJLabel = new javax.swing.JLabel();
        introJLabel = new javax.swing.JLabel();
        startJBtn = new javax.swing.JButton();
        stopJBtn = new javax.swing.JButton();
        middleJBtn = new javax.swing.JButton();
        lookJLabel = new javax.swing.JLabel();
        lookJLabel1 = new javax.swing.JLabel();
        lookJLabel2 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(2160, 1300));
        setMinimumSize(new java.awt.Dimension(2160, 1300));
        setPreferredSize(new java.awt.Dimension(2160, 1300));

        titleJLabel.setFont(new java.awt.Font("Helvetica", 1, 56)); // NOI18N
        titleJLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJLabel.setText("Anti-Saccade Test");
        titleJLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titleJLabel.setMaximumSize(new java.awt.Dimension(1600, 70));
        titleJLabel.setMinimumSize(new java.awt.Dimension(1600, 70));
        titleJLabel.setPreferredSize(new java.awt.Dimension(1600, 70));
        titleJLabel.setRequestFocusEnabled(false);

        introJLabel.setFont(new java.awt.Font("Helvetica", 1, 48)); // NOI18N
        introJLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        introJLabel.setText("Follow the instruction bellow to finish the Trapezoid Test.");
        introJLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        introJLabel.setMaximumSize(new java.awt.Dimension(1600, 70));
        introJLabel.setMinimumSize(new java.awt.Dimension(1600, 70));
        introJLabel.setPreferredSize(new java.awt.Dimension(1600, 70));
        introJLabel.setVerifyInputWhenFocusTarget(false);

        startJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        startJBtn.setText("START");
        startJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        startJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        startJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        startJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        startJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startJBtnActionPerformed(evt);
            }
        });

        stopJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        stopJBtn.setText("STOP");
        stopJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stopJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        stopJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        stopJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        stopJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopJBtnActionPerformed(evt);
            }
        });

        middleJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        middleJBtn.setText("NEXT");
        middleJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        middleJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        middleJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        middleJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        middleJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                middleJBtnActionPerformed(evt);
            }
        });

        lookJLabel.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        lookJLabel.setForeground(new java.awt.Color(255, 0, 0));
        lookJLabel.setText("Look Here");
        lookJLabel.setMaximumSize(new java.awt.Dimension(210, 50));
        lookJLabel.setMinimumSize(new java.awt.Dimension(210, 50));
        lookJLabel.setPreferredSize(new java.awt.Dimension(210, 50));

        lookJLabel1.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        lookJLabel1.setForeground(new java.awt.Color(255, 0, 0));
        lookJLabel1.setText("Look Here");
        lookJLabel1.setMaximumSize(new java.awt.Dimension(210, 50));
        lookJLabel1.setMinimumSize(new java.awt.Dimension(210, 50));
        lookJLabel1.setPreferredSize(new java.awt.Dimension(210, 50));

        lookJLabel2.setFont(new java.awt.Font("Tahoma", 1, 40)); // NOI18N
        lookJLabel2.setForeground(new java.awt.Color(255, 0, 0));
        lookJLabel2.setText("Look Here");
        lookJLabel2.setMaximumSize(new java.awt.Dimension(210, 50));
        lookJLabel2.setMinimumSize(new java.awt.Dimension(210, 50));
        lookJLabel2.setPreferredSize(new java.awt.Dimension(210, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(280, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(startJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(middleJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(stopJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(titleJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(introJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(280, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(lookJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lookJLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lookJLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(titleJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(introJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(310, 310, 310)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lookJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lookJLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lookJLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stopJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(middleJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(200, 200, 200))
        );
    }// </editor-fold>//GEN-END:initComponents

   private void startJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startJBtnActionPerformed
       // TODO add your handling code here:
       instruction = false;
       middleJBtn.setVisible(false);
       introJLabel.setText("Go go go!");
       leftToShow = (int) presentationsPerSide;
       rightToShow = (int) presentationsPerSide;
       leftPulsing = false;
       rightPulsing = false;
       antisaccadeTask.setIsDone(false);
       long tempTime = System.currentTimeMillis();
       while ((System.currentTimeMillis() - tempTime) < 1000) {
           //System.out.println("Hold 1 s before start.");
       }
       lineDataSet.removeAllSeries();
       lineDataSets.clear();
       dataWriter = new DataWriter(gm, antisaccadeTask, patient);
       testStart = new Date();
       startTestTimer(1000);
       gm.addGazeListener(this);
   }//GEN-LAST:event_startJBtnActionPerformed

   private void stopJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopJBtnActionPerformed
       testTimer.stop();
       pulseTimer.stop();
       antisaccadeTask.setIsDone(true);
   }//GEN-LAST:event_stopJBtnActionPerformed

    private void middleJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_middleJBtnActionPerformed
        if (middleJBtn.getText().equals("NEXT")) {
            //  Next function
            manualStopPulse();
            displayStartScreen();
        }
        else {
            //  Restart instructions function
            startJBtn.setVisible(false);
            stopJBtn.setVisible(false);
            middleJBtn.setText("NEXT");
            middleJBtn.setMaximumSize(new Dimension(180, 50));
            middleJBtn.setMinimumSize(new Dimension(180, 50));
            middleJBtn.setPreferredSize(new Dimension(180, 50));

            instructionTimer = new java.util.Timer();
            instructionsPhaseOne(false);
        }
    }//GEN-LAST:event_middleJBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel introJLabel;
    private javax.swing.JLabel lookJLabel;
    private javax.swing.JLabel lookJLabel1;
    private javax.swing.JLabel lookJLabel2;
    private javax.swing.JButton middleJBtn;
    private javax.swing.JButton startJBtn;
    private javax.swing.JButton stopJBtn;
    private javax.swing.JLabel titleJLabel;
    // End of variables declaration//GEN-END:variables

    private void manualPulseLeft () {
        leftToShow = 100;
        rightToShow = 0;
        leftPulsing = leftPulsingFreeze = true;
        rightPulsing = rightPulsingFreeze = centerPulsingFreeze = false;
    }
    private void manualPulseCenter () {
        leftPulsingFreeze = leftPulsing = false;
        rightPulsingFreeze = rightPulsing = false;
        centerPulsingFreeze = true;
    }
    private void manualPulseRight () {
        leftToShow = 0;
        rightToShow = 100;
        leftPulsing = leftPulsingFreeze = centerPulsingFreeze = false;
        rightPulsing = rightPulsingFreeze = true;
    }
    private void manualStopPulse () {
        rightPulsing = rightPulsingFreeze = false;
        leftPulsing = leftPulsingFreeze = false;
        centerPulsingFreeze = false;

        pulseWidth = 0;
        repaint();
        if (testTimer != null) { testTimer.stop(); }
        if (pulseTimer != null) { pulseTimer.stop(); }
    }

    private void instructionsPhaseOne(boolean isRepeat) {
        instruction = true;

        if (!isRepeat) {
            initiateAnimationParams();
            introJLabel.setText("In this task, you will look AWAY from the pulsing target");
            instructionTimer.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        instructionsPhaseTwo();
                    }
                },
                5000
            );
        }
        else {
            instructionsPhaseTwo();
        }
    }

    private void instructionsPhaseTwo() {
        //  No pulse
        introJLabel.setText("When there is no pulsing target, look at the center target");
        lookJLabel1.setVisible(true);
        manualStopPulse();

        //   Schedule the other pulses
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run() {
                    //   Pulse the left dot
                    introJLabel.setText("Look AWAY from the pulsing target");
                    lookJLabel.setVisible(false);
                    lookJLabel1.setVisible(false);
                    lookJLabel2.setVisible(true);
                    manualPulseLeft();
                    startTestTimer(1000);
                    pulseTimer.start();
                }
            },
            5000
        );
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run() {
                    //   No pulse
                    introJLabel.setText("When there is no pulsing target, look at the center target");
                    lookJLabel.setVisible(false);
                    lookJLabel1.setVisible(true);
                    lookJLabel2.setVisible(false);
                    manualStopPulse();
                }
            },
            10000
        );
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run() {
                    //   Pulse the right dot
                    introJLabel.setText("Look AWAY from the pulsing target");
                    lookJLabel.setVisible(true);
                    lookJLabel1.setVisible(false);
                    lookJLabel2.setVisible(false);
                    manualPulseRight();
                    startTestTimer(1000);
                    pulseTimer.start();
                }
            },
            15000
        );
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run() {
                    lookJLabel.setVisible(false);
                    lookJLabel1.setVisible(false);
                    lookJLabel2.setVisible(false);
                    manualStopPulse();
                    instructionsPhaseOne(true); //  Repeat instructions
                }
            },
            20000
        );
    }

    private void displayStartScreen() {
        instructionTimer.cancel();
        lookJLabel.setVisible(false);
        lookJLabel1.setVisible(false);
        lookJLabel2.setVisible(false);
        startJBtn.setVisible(true);
        stopJBtn.setVisible(true);

        //  Change middle button to restart instructions
        middleJBtn.setText("RESTART INSTRUCTIONS");
        middleJBtn.setMaximumSize(new Dimension(300, 50));
        middleJBtn.setMinimumSize(new Dimension(300, 50));
        middleJBtn.setPreferredSize(new Dimension(300, 50));

        instruction = false;
        antisaccadeTask.setIsDone(true);
        introJLabel.setText("When you're ready, click the start button to begin the test");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //If shit is pulsing, stop it, decrease counters, check to see if we are done,  if not done, refocus!
        if (leftPulsing || rightPulsing) {
            xLoc = xLocC;         //When focusing, the position returned is that of the center target
            if (leftPulsing) {
                leftPulsing = leftPulsingFreeze;
                leftToShow--;
            }
            if (rightPulsing) {
                rightPulsing = rightPulsingFreeze;
                rightToShow--;
            }
            pulseAngle = 180;
            repaint();
            if (leftToShow + rightToShow == 0) {   //Stop if we are done!
                antisaccadeTask.setIsDone(true);
                testTimer.stop();
                pulseTimer.stop();
                if (!instruction) {
                    roundFinished();
                }
                this.testFinished();
                return;
            } else {					//if not done, set new delay
                currentResolution = randInt((int) focusTimeLow, (int) focusTimeHigh);
                testTimer.setInitialDelay(currentResolution);
                testTimer.start();
            }

        } else {
            //Choose Object to pulse
            double leftProb = (double) leftToShow / ((double) leftToShow + (double) rightToShow); //find out odds of it being L or R
            double randSample = Math.random();
            //System.out.println("Rand Sample is " + randSample + ", Left Prob is " + leftProb);
            if (randSample < leftProb) {  //choose a random number 0-1
                leftPulsing = !centerPulsingFreeze;
                xLoc = xLocL;
            } else {
                rightPulsing = !centerPulsingFreeze;
                xLoc = xLocR;

            }
            //Start Timing until Pulsing will be done
            testTimer.setInitialDelay((int) pulseTime);
            testTimer.start();
            //StartPulsing
            pulseAngle = 180;
            pulseTimer.start();
        }
        if (!instruction) {
            roundFinished();
        }
    }

    private void roundFinished() {
        try {
            //Stop if we are done!
            synchronized (lock) {
                XYSeries gazeSeries = (XYSeries) gazeLine.clone();
                XYSeries targetSeries = (XYSeries) targetLine.clone();
                lineDataSet.addSeries(gazeSeries);
                lineDataSet.addSeries(targetSeries);
                XYSeriesCollection newCollection = (XYSeriesCollection) lineDataSet.clone();
                lineDataSets.add(newCollection);
                gazeLine.clear();
                targetLine.clear();
                lineDataSet.removeAllSeries();
            }
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(AntiSaccadeTestJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onGazeUpdate(GazeData gd) {

        long elpasedtime = gd.timeStamp - testStart.getTime();
        long systemTime = System.currentTimeMillis() - testStart.getTime();
        String dataStr = "";

        int x = (int) (xLoc);
        int y = (int) (yLoc + 140);
        dataStr = dataStr + x + "," + y + "," + x + "," + y + ",";
        dataStr = dataStr + gd.rawCoordinates.x + "," + gd.rawCoordinates.y + "," + gd.leftEye.rawCoordinates.x + "," + gd.leftEye.rawCoordinates.y + "," + gd.rightEye.rawCoordinates.x + "," + gd.rightEye.rawCoordinates.y + "," + elpasedtime + "," + systemTime;
        if (elpasedtime >= 0.0 && elpasedtime <= 1e6) {
            synchronized (lock) {
                targetLine.add((double) elpasedtime, (double) x);
                gazeLine.add((double) elpasedtime, (double) gd.rawCoordinates.x);
            }
            recordedTimes++;
        } else {
            errorTimes++;
        }
        dataWriter.updateGazeData(dataStr);

        if (antisaccadeTask.isIsDone()) {
            gm.clearListeners();
            dataWriter.finishWriter();
        }
    }

    private class PulseTimerAction implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            pulseAngle = pulseAngle + (double) pulseResolution * pulses * 360.0 / (double) pulseTime;
            //System.out.println("Pulse Angle is " + pulseAngle);
            pulseWidth = pulseWidthMax * (Math.cos(Math.toRadians(pulseAngle)) + 1);
            repaint();
        }
    }
}
