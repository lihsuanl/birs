/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.TrapezoidTest;

import Business.Business.Business;
import Business.DataWriter.DataWriter;
import Business.Patient.Patient;
import Business.Task.SmoothTrapezoidTask;
import UserInterface.TestResult.TestResultJPanel;
import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.IGazeListener;
import com.theeyetribe.client.data.GazeData;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author neethan
 */
public class TrapezoidTestJPanel extends javax.swing.JPanel implements ActionListener, IGazeListener {

    /**
     * Creates new form TrapezoidTestJPanel
     */
    private JPanel userProcessContainer;
    private Business business;
    private Patient patient;
    private SmoothTrapezoidTask trapezoidTask;
    private GazeManager gm;
    private BufferedImage image;
    private DataWriter dataWriter;

    //=== Above are the BUSINESS Part Parameters. Following are UserInterface Part Parameters. ===//
    private int xCenter;
    private int yCenter;
    private int moveMaxX;
    private int moveX;
    private int moveY;
    private int currentResolution;
    private long cycleStart;
    private Date testStart;
    private int cycleTime;
    private int animateTime;
    private int round;
    private int currentCycleTimeIndex;
    private int pulseTime;
    private float pulseFraction;
    private float animateFraction;
    private Timer demoTimer;
    private Timer testTimer;
    private XYSeries targetLine = new XYSeries("Target");
    private XYSeries gazeLine = new XYSeries("Gaze");
    private XYSeriesCollection lineDataSet = new XYSeriesCollection();
    private ArrayList<XYSeriesCollection> lineDataSets = new ArrayList<XYSeriesCollection>();
    private int errorTimes = 0;
    private int recordedTimes = 0;
    private boolean instruction = true;
    private boolean isPaused = false;
    private java.util.Timer instructionTimer;
    private final Object lock = new Object();

    public TrapezoidTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.patient = patient;
        this.gm = gm;
        trapezoidTask = new SmoothTrapezoidTask();
        instructionTimer = new java.util.Timer();
        image = null;

        startJBtn.setVisible(false);
        stopJBtn.setVisible(false);
        middleJBtn.setVisible(true);
        instructionsPhaseOne();
    }

    private void initiateAnimationParams() {
        xCenter = this.getPreferredSize().width / 2;
        yCenter = this.getPreferredSize().height / 2;
        // yCenter = 50;
        moveMaxX = this.getPreferredSize().width * 3 / 7;
        moveX = xCenter - moveMaxX;
        moveY = yCenter;
        pulseTime = 500;
        currentResolution = 0;
    }

    private void instructionsPhaseOne () {
        instruction = true;

        initiateAnimationParams();
        introJLabel.setText(asHtmlCenteredString(
            "This task is similar to the last task. Follow the moving target on the screen with your eyes."
            + " Please keep your head as still as possible while following the target with your eyes"
        ));
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run () {
                    instructionsPhaseTwo();
                }
            },
            5000
        );
    }
    private void instructionsPhaseTwo () {
        introJLabel.setText("This is a demonstration of the target movement");
        startTestTimer(currentResolution);
    }

    private void createAnimationImage() {
        int imageW = 20;
        int imageH = 20;
        GraphicsConfiguration gc = getGraphicsConfiguration();
        image = gc.createCompatibleImage(imageW, imageH, Transparency.TRANSLUCENT);
        Graphics2D gImg = image.createGraphics();
        Color graphicsColor = Color.BLACK;
        gImg.setColor(graphicsColor);
        gImg.fillOval(0, 0, imageW, imageH);
        gImg.dispose();
    }

    @Override
    public void paintComponent(Graphics g) {
        if (image == null) {
            createAnimationImage();
        }
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.drawImage(image, moveX, moveY, null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleJLabel = new javax.swing.JLabel();
        introJLabel = new javax.swing.JLabel();
        startJBtn = new javax.swing.JButton();
        stopJBtn = new javax.swing.JButton();
        middleJBtn = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(2160, 1300));
        setMinimumSize(new java.awt.Dimension(2160, 1300));
        setPreferredSize(new java.awt.Dimension(2160, 1300));

        titleJLabel.setFont(new java.awt.Font("Helvetica", 1, 56)); // NOI18N
        titleJLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJLabel.setText("Trapezoid Test");
        titleJLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titleJLabel.setMaximumSize(new java.awt.Dimension(750, 70));
        titleJLabel.setMinimumSize(new java.awt.Dimension(750, 70));
        titleJLabel.setPreferredSize(new java.awt.Dimension(750, 70));

        introJLabel.setFont(new java.awt.Font("Helvetica", 1, 48)); // NOI18N
        introJLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        introJLabel.setText("Follow the instruction bellow to finish the Trapezoid Test.");
        introJLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        introJLabel.setMaximumSize(new java.awt.Dimension(1600, 200));
        introJLabel.setMinimumSize(new java.awt.Dimension(1600, 200));
        introJLabel.setPreferredSize(new java.awt.Dimension(1600, 200));

        startJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        startJBtn.setText("START");
        startJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        startJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        startJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        startJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        startJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startJBtnActionPerformed(evt);
            }
        });

        stopJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        stopJBtn.setText("STOP");
        stopJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        stopJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        stopJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        stopJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        stopJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopJBtnActionPerformed(evt);
            }
        });

        middleJBtn.setFont(new java.awt.Font("Helvetica", 0, 20)); // NOI18N
        middleJBtn.setText("NEXT");
        middleJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        middleJBtn.setMaximumSize(new java.awt.Dimension(180, 50));
        middleJBtn.setMinimumSize(new java.awt.Dimension(180, 50));
        middleJBtn.setPreferredSize(new java.awt.Dimension(180, 50));
        middleJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                middleJBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(280, Short.MAX_VALUE)
                .addComponent(introJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(280, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(startJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(middleJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(stopJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(titleJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(titleJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(introJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(746, 746, 746)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(middleJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stopJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startJBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(100, 100, 100))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void startJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startJBtnActionPerformed
        if (isPaused) {
            startTestTimer(currentResolution);
            isPaused = false;
            return;
        }

        cycleTime = trapezoidTask.getCycleTimeSet()[0] + 2 * pulseTime;
        round = 1;
        currentCycleTimeIndex = 0;
        trapezoidTask.setIsDone(false);
        instruction = false;
        instructionTimer.schedule(
            new java.util.TimerTask() {
                @Override
                public void run () {
                    startImpl();
                }
            },
            1000
        );
    }//GEN-LAST:event_startJBtnActionPerformed
    private void startImpl () {
        instructionTimer.cancel();
        middleJBtn.setVisible(false);
        introJLabel.setText("");

        animateTime = trapezoidTask.getCycleTimeSet()[0];
        pulseFraction = (float) pulseTime / cycleTime;
        animateFraction = (float) animateTime / (2 * cycleTime);

        cycleStart = System.nanoTime() / 1000000;
        lineDataSet.removeAllSeries();
        lineDataSets.clear();
        testStart = new Date();
        dataWriter = new DataWriter(gm, trapezoidTask, patient);
        startTestTimer(currentResolution);
        gm.addGazeListener(this);
    }

    private void stopJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopJBtnActionPerformed
        isPaused = true;
        stopTestTimer();
        trapezoidTask.setIsDone(true);
    }//GEN-LAST:event_stopJBtnActionPerformed

    private void middleJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_middleJBtnActionPerformed
       if (middleJBtn.getText().equals("NEXT")) {
           //  Next function
           displayStartScreen();
       }
       else {
           //  Restart instructions function
           startJBtn.setVisible(false);
           stopJBtn.setVisible(false);
           middleJBtn.setText("NEXT");
           middleJBtn.setMaximumSize(new Dimension(180, 50));
           middleJBtn.setMinimumSize(new Dimension(180, 50));
           middleJBtn.setPreferredSize(new Dimension(180, 50));

           instructionsPhaseOne();
       }
    }//GEN-LAST:event_middleJBtnActionPerformed

    private void displayStartScreen () {
        stopTestTimer();
        instructionTimer.cancel();
        instructionTimer = new java.util.Timer();

        startJBtn.setVisible(true);
        stopJBtn.setVisible(true);

        //  Move dot back to starting position
        moveX = (int) xCenter - moveMaxX;
        moveY = (int) yCenter;
        repaint();

        //  Change middle button to restart instructions
        middleJBtn.setText("RESTART INSTRUCTIONS");
        middleJBtn.setMaximumSize(new Dimension(300, 50));
        middleJBtn.setMinimumSize(new Dimension(300, 50));
        middleJBtn.setPreferredSize(new Dimension(300, 50));

        introJLabel.setText(asHtmlCenteredString(
            "When you're ready, click the start button to begin the test"
        ));
    }

    private void startTestTimer(int resolution) {
        if (testTimer != null) {
            testTimer.stop();
            testTimer.setDelay(resolution);
        } else {
            testTimer = new Timer(resolution, this);
        }

        testTimer.start();
        trapezoidTask.setIsDone(false);
    }
    private void stopTestTimer () {
        if (testTimer != null) {
            testTimer.stop();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel introJLabel;
    private javax.swing.JButton middleJBtn;
    private javax.swing.JButton startJBtn;
    private javax.swing.JButton stopJBtn;
    private javax.swing.JLabel titleJLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        long currentTime = System.nanoTime() / 1000000;
        long totalTime = currentTime - cycleStart;

        if (totalTime > cycleTime) {
            cycleStart = currentTime;

            if (round < trapezoidTask.getNoOfCycle()) {
                round++;
                if (!instruction) {
                    roundFinished();
                }
            } else if (currentCycleTimeIndex + 1 >= trapezoidTask.getCycleTimeSet().length) {
                cycleTime = Integer.MIN_VALUE;
                round = Integer.MAX_VALUE;
                currentCycleTimeIndex = Integer.MAX_VALUE;
                moveX = (int) xCenter - moveMaxX;
                moveY = (int) yCenter;
                trapezoidTask.setIsDone(true);
                testTimer.stop();
                if (!instruction) {
                    this.roundFinished();
                    this.testFinished();
                }
                startJBtn.setVisible(true);
                stopJBtn.setText("STOP");
                instruction = false;
                repaint();
            } else {
                currentCycleTimeIndex = instruction ? 0 : currentCycleTimeIndex + 1;
                round = 1;
                animateTime = trapezoidTask.getCycleTimeSet()[currentCycleTimeIndex];
                cycleTime = trapezoidTask.getCycleTimeSet()[currentCycleTimeIndex] + 2 * pulseTime;
                pulseFraction = (float) pulseTime / cycleTime;
                animateFraction = (float) animateTime / (2 * cycleTime);
                if (!instruction) {
                    roundFinished();
                }
            }
        }
        moveX = (int) (xCenter + (int) (getOffset(totalTime, cycleTime)));
        repaint();
    }

    private void roundFinished() {
        try {
            synchronized (lock) {
                XYSeries gazeSeries = (XYSeries) gazeLine.clone();
                XYSeries targetSeries = (XYSeries) targetLine.clone();
                lineDataSet.addSeries(gazeSeries);
                lineDataSet.addSeries(targetSeries);
                XYSeriesCollection newCollection = (XYSeriesCollection) lineDataSet.clone();
                lineDataSets.add(newCollection);
                gazeLine.clear();
                targetLine.clear();
                lineDataSet.removeAllSeries();
            }
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(TrapezoidTestJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private float getOffset(long totalTime, long cycleTime) {
        float fraction = (float) totalTime / cycleTime;
        fraction = Math.min(1.0f, fraction);
        fraction = Math.max(0.0f, fraction);
        float offset = 0;

        if (fraction < animateFraction) {
            offset = .5f + (2 * fraction / animateFraction - 1) * (float) moveMaxX;
        } else if (fraction >= animateFraction && fraction < animateFraction + pulseFraction) {
            offset = (float) moveMaxX;
        } else if (fraction >= animateFraction + pulseFraction && fraction < 2 * animateFraction + pulseFraction) {
            offset = .5f + ((2 * pulseFraction - 2 * fraction) / animateFraction + 3) * (float) moveMaxX;
        } else if (fraction >= 2 * animateFraction + pulseFraction) {
            offset = -(float) moveMaxX;
        }

        return offset;
    }

    private void testFinished() {
        trapezoidTask.setResultTitle("Precentage Recorded: " + (double) recordedTimes / (recordedTimes + errorTimes));
        TestResultJPanel trjp = new TestResultJPanel(userProcessContainer, business, patient, gm, trapezoidTask, lineDataSets);
        userProcessContainer.add("Trapezoid Test Result JPanel", trjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }

    @Override
    public void onGazeUpdate(GazeData gd) {
        // TODO store both target location and gaze location
        long elpasedtime = gd.timeStamp - testStart.getTime();
        long systemTime = System.currentTimeMillis() - testStart.getTime();
        String dataStr = "";

        int x = moveX + 10;
        int y = moveY + 140 + 10;
        dataStr = dataStr + x + "," + y + "," + x + "," + y + ",";
        dataStr = dataStr + gd.rawCoordinates.x + "," + gd.rawCoordinates.y + "," + gd.leftEye.rawCoordinates.x + "," + gd.leftEye.rawCoordinates.y + "," + gd.rightEye.rawCoordinates.x + "," + gd.rightEye.rawCoordinates.y + "," + elpasedtime + "," + systemTime + "," + this.round + "," + this.currentCycleTimeIndex;
        if (elpasedtime >= 0.0 && elpasedtime <= 1e6) {
            synchronized (lock) {
                targetLine.add((double) elpasedtime, (double) x);
                gazeLine.add((double) elpasedtime, (double) gd.rawCoordinates.x);
            }
            recordedTimes++;
        } else {
            errorTimes++;
        }
        dataWriter.updateGazeData(dataStr);
        if (trapezoidTask.isIsDone()) {
            gm.clearListeners();
            dataWriter.finishWriter();
        }
    }

    private String asHtmlCenteredString (String s) {
        return "<html><p style='text-align: center'>" + s + "</p></html>";
    }
}
