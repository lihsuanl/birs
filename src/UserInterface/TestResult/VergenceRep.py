import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import math

from scipy.optimize import curve_fit
from scipy.stats import distributions

class VergenceRep:


    def __init__(self,rawRepData,repParseData):
        self.rawRepData = rawRepData
        self.repParseData = repParseData
        #print(rawRepData)
    def calcMetric(self,metricName):
        if ~metricName.find('con-tau'):
            metricVal = self.calcTau('con')
        elif ~metricName.find('div-tau'):
            metricVal = self.calcTau('div')
        elif ~metricName.find('latency'):
            metricVal = self.calcLatency
        else:
            print(metricName + ' is not a defined metric for Vergence Rep')
        return metricVal
    
    def plotMetric(self,metricName):
        if ~metricName.find('con-tau') | ~metricName.find('div-tau'):
            h = self.plotTau()
        else:
            print(metricName + ' is not a defined metric for VergenceRep')
            
    def calcTau(self,movement):
        if self.repParseData == 1:
            #print(self.rawRepData['type'])
            #print(movement)
            if (self.rawRepData['type'].find(movement)+1):
                #print('hi')
                inds = ~np.isnan(self.rawRepData['data'])
                data = self.rawRepData['data'][inds]
                t = self.rawRepData['t'][inds]
                inds = ~np.isnan(t)
                data = data[inds]
                t = t[inds]
                if len(data) > 2:
                    pars,pcov = self.returnFit(t,data)
                    #Calculate confidence intervals
                    alpha = 0.05
                    n = len(data)
                    p = len(pars)
                    dof = max(0,n-p)
                    tval = distributions.t.ppf(1-alpha/2,dof)
                    conf_p1plus = pars[0]+pcov[0][0]*tval
                    conf_p2plus = pars[1]+pcov[1][1]*tval
                    score = np.abs(pars[1]/(conf_p1plus-conf_p2plus))
                    tau = -1000/pars[1]
                    #print(score)
                    #print(tau)
                    if tau < 15:
                        tau = float('NaN')
                    if tau > 500:
                        tau = float('NaN')
                    if score > 8.5:
                        tau = float('NaN')
                else:
                    tau = float('NaN')
            else:
                tau = float('NaN')
        else:
            tau = float('NaN')
        #print(str(tau) + ' hi')
        return tau        
                    
                
    def func(self,x,p1,p2):
        return p1*np.exp(p2*x)
    
                
    def returnFit(self,t,data):
        #If the rep is good, return the fit
        #Else return NaN
        inds = ~np.isnan(self.rawRepData['data'])
        data = self.rawRepData['data'][inds]
        t = self.rawRepData['t'][inds]
        inds = ~np.isnan(t)
        data = data[inds]
        t = t[inds]
        #print(t)
        #Fitting exponential to linear by taking log
        #b = np.log(t[1:(len(t)-1)])
        #f = np.polyfit(b,np.log(data[1:(len(data)-1)]),1)
        f,f1 = curve_fit(self.func,t,data)
        #print(t)
        #print(data)
        #print(f)
        return f,f1                
                
    def calcLatency(self):
        blah = 0
        
    
    def plotTau(self):
        if self.repParseData == 1:
            inds = ~np.isnan(self.rawRepData['data'])
            data = self.rawRepData['data'][inds]
            t = self.rawRepData['t'][inds]
            inds = ~np.isnan(t)
            data = data[inds]
            y_fit = []
            if len(data) > 2:
                pars,pcovs = self.returnFit(t,data)
                y_fit = self.func(t,pars[0],pars[1])
                h = plt.plot(t,y_fit,linewidth=1,color='r')
            else:
                h = plt.plot([])
                
        else:
            h = plt.plot([])
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
