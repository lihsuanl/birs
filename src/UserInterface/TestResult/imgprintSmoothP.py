#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math
import os

from readTrial import readTrial
from loadtrial import loadTrial
from parseSmoothPData import parseSmoothPData
from SmoothPRep import SmoothPRep
from SmoothPSinFit import SmoothPSinFit
#from sinuFit import sinuFit

import matplotlib.pyplot as plt


def imgprintSmoothP(fileName,trialType,output_dir):

    parseParams = {}
    parseParams['minDataPoints'] = 5  #this is the minimum amount of data points per rep to be counted for this to be counted as a trial!
    parseParams['minPercentRecorded'] = .9
    parseParams['maxAvDT'] = .04
    parseParams['skipSinFit'] = 0

    ## Intermediate Steps
    #load trial Data
    data = loadTrial(fileName,trialType);
    #parse Data
    [reps,repsParseData]=parseSmoothPData(data,parseParams);

    

    ## plot Fitting Results of all Reps
    ax3 = plt.figure()
    nSpeeds = 4
    nReps = 10
    ax3.suptitle('phLag rsq')
    for i in range(nSpeeds*nReps):
        ax = plt.subplot(nSpeeds,nReps,i+1)
        reps[i].rawRepData['sinFit'].plotPhaseFit()
        ax.axes.get_xaxis().set_ticks([])
        ax.axes.get_yaxis().set_ticks([])
        plt.tight_layout()
    plt.subplots_adjust(top=0.85)
    plt.subplots_adjust(top=0.85)
    imgname = fileName[:-4] + 'image.png'
    savedname = os.path.join(str(output_dir) + imgname)
    print(savedname)
    ax1.savefig(savedname)
    return 'plot=' + savedname

