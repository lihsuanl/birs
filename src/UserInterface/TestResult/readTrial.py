#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import matplotlib.pyplot as plt

from pix2degrees import pix2degrees

from func1 import process_options

def val_confirm(**kwargs):
    return kwargs

def readTrial(f, nTargets, extra_col_names, **kwargs):
    if kwargs != {}:
        offSetType_new = val_confirm(**kwargs)
        offSetType = offSetType_new['offSetType']
    else:
        offSetType = 'firstVal'
    
    
    #first parse off parameters
    f1 = open(f)
    fid = csv.reader(f1)

    lin_count = 0
    exit_param_read = 0
    invalid_chars = '() ;-/.:^'
    param = dict()
    param_count = 0
    width = 0
    for row in fid:
        #print(row)
        #print(row[1])
        lin_count += 1
        #first two if are to see if all parameters read
        
        if 'End of Parameters ' in row:
            param_count = lin_count
        if '-1' in row:
            param_count = lin_count
        if param_count == 0:
            pN = row[0]
            for i in invalid_chars:
                pN = pN.replace(i,'')
            
            pV = row[1]
            param[pN] = pV
        elif lin_count > param_count:
            width = len(row)
            
    height = lin_count - param_count - 1
    #height = lin_count - param_count - 2
    #print(height)
    M = [[0 for x in range(width)] for y in range(height)] 
    #print(str(len(M)) + ' length')
    lin_count = 0
    val_count = 0
    #opening file again to read in values
    f2 = open(f)
    cid = csv.reader(f2)
    for row in cid:
        lin_count += 1
        if lin_count > (param_count+1):
        #if lin_count > (param_count+2):
            M[val_count][:] = row
            val_count += 1
            
    M = numpy.array(M)
    #print(str(len(M[:,1])) + ' length')
    #print(M[2390,:])
            
    if nTargets==2:
        col_Ns=['Ob1X','Ob1Y','Ob2X','Ob2Y','GazeX','GazeY','LEyeX','LEyeY','REyeX','REyeY','T']
        if extra_col_names != []:
            for i in extra_col_names:
                col_Ns.append(i)
        i_t=10
        x_off_cols = [0,2,4,6,8]
        y_off_cols = [1,3,5,7,9]
        pos_cols = list(range(10))
    elif nTargets == 1:
        col_Ns=['Ob1X','Ob1Y','GazeX','GazeY','T'];
        if extra_col_names != []:
            for i in extra_col_names:
                col_Ns.append(i)
        x_off_cols = [0,2];
        y_off_cols = [1,3];
        i_t=4;
        pos_cols = list(range(4));
    
    #if f == 'SmoothTrapezoidTask269907097313375_Output.csv':
    #    for i in range(10):
    #        print(M[i+2219-5,i_t])
    #        print(str(i+2219-5))    
    
    #remove nonsensical time values
    trial = {}
    trial['N_unrealistic_t'] = len([i for i in M[:,i_t] if int(float(i)) > 1e6])
    #print(trial['N_unrealistic_t'])
    #print('how are you')
    trial['N_original'] = len(M) - lin_count - 1
    
    #remove overly large values
    indexes = [index for index, value in enumerate(M[:,i_t]) if abs(int(float(value))) < 1e6]
    K = numpy.ndarray(shape = (len(indexes),len(M[1,:])))
    
    lin_num = 0
    for i in range(len(M)):
        if i == indexes[lin_num]:
            K[lin_num,:] = M[i,:]
            lin_num += 1
    
    #remove negative values
    indexes = [index for index, value in enumerate(K[:,i_t]) if int(float(value)) > 0]
    N = numpy.ndarray(shape = (len(indexes),len(K[1,:])))
        
    lin_num = 0
    for i in range(len(K)):
        if i == indexes[lin_num]:
            N[lin_num,:] = K[i,:]
            lin_num += 1
    
    
    
    #T = numpy.array(N)
    #iteratively remove time value that are not sequential
    t = N[:,i_t]/1000 
    dt = numpy.diff(t)
    trial['N_DecreasingT'] = 0


    while max(abs(dt)) > 1:
        #print(max(abs(dt)))
        bad_ind = [index for index, value in enumerate(dt) if abs(value) == max(abs(dt))]
        #print(bad_ind)
        bad_ind_low = [x-1 for x in bad_ind]
        if t[bad_ind_low] < t[bad_ind]:
            bad_ind = [x+1 for x in bad_ind]
        remove_neg_dt_inds = numpy.setdiff1d(numpy.arange(len(t)),bad_ind)
        N_neg_or_zero_inds = len([index for index, value in enumerate(dt) if value == max(abs(dt))]) #what is this for
        N1 = numpy.ndarray(shape = (len(N)-N_neg_or_zero_inds,len(M[1,:])))
        lin_num = 0
        lin_num2 = 0
        for i in range(len(N)):
            if i != remove_neg_dt_inds[lin_num]:
                N1[lin_num2,:] = N[i,:]
                lin_num2 += 1
            else:
                lin_num += 1
        N = numpy.copy(N1) 
        t = N[:,i_t]/1000 
        dt = numpy.diff(t)
        trial['N_DecreasingT'] += N_neg_or_zero_inds
    while min(dt) <=0:
        remove_neg_dt_inds = [index for index, value in enumerate(dt) if value != min(dt)]
        N_neg_or_zero_inds = len([index for index, value in enumerate(dt) if value == min(dt)])
        N1 = numpy.ndarray(shape = (len(N_neg_or_zero_inds),len(M[1,:])))
        lin_num = 0
        for i in range(len(N)):
            if i == remove_neg_dt_inds[lin_num]:
                N1[lin_num,:] = N[i,:]
                lin_num += 1
        N = numpy.copy(N1) 
        t = N[:,i_t]/1000 
        dt = numpy.diff(t)
        trial['N_DecreasingT'] += N_neg_or_zero_inds  
      
    #turn 0 values into NaN values
    N[N==0] = float('NaN')       
    if offSetType == 'firstVal':
        xOffset = N[0,0]
    elif offSetType == 'mean':
        xOffset = numpy.mean([max(N[:,0]), min(N[:,0])])
        
    else:
        print('Invalid offset type provided!')
    
    yOffset = N[0,1]
    N[:,x_off_cols] -= xOffset
    N[:,y_off_cols] -= yOffset
    

    
    try:
        N[:,pos_cols] = pix2degrees(N[:,pos_cols],float(param['screenRes']),float(param['ScreenDistIn']))
    except KeyError:
        N[:,pos_cols] = pix2degrees(N[:,pos_cols],float(param['screenRes']),float(param['ScreenDistanceIn']))
        
    for i in range(len(col_Ns)):
        
        name = col_Ns[i]
        #print(i)
        #print(N[:,i])
        trial[str(name)] = N[:,i]
        #print(trial[str(name)].shape)
        #print(name)

    percent_recorded = {}
    trial['percent_recorded'] = percent_recorded
    trial['percent_recorded']['GX'] = 100 - len([index for index,value in enumerate(trial['GazeX']) if numpy.isnan(value) == True])/len(trial['GazeX'])*100
    if nTargets == 2:
        trial['percent_recorded']['LX'] = 100 - len([index for index,value in enumerate(trial['LEyeX']) if numpy.isnan(value) == True])/len(trial['LEyeX'])*100
        trial['percent_recorded']['RX'] = 100 - len([index for index,value in enumerate(trial['REyeX']) if numpy.isnan(value) == True])/len(trial['REyeX'])*100

    trial['params'] = param
    trial['T'] /= 1000
    

    
    f1.close()
    f2.close()
    trial['f'] = f
    s = trial['f']
    ind1 = s.find('Output')+6
    ind2 = s.find('.csv')-1
    try:
        trial['Trec'] = int(s[ind1:ind2])
    except ValueError:
        trial['Trec'] = []
    #trial['Trec'] = int(s[ind1:ind2])
    return trial

#end of function    
    


      

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        