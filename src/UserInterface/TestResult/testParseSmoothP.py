#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseSmoothPData import parseSmoothPData 

import matplotlib.pyplot as plt

#Input arguments to create a Trial object that of type DistanceVergenceTask
fileName='SmoothDiscreteSpeedLineTask269808029650582_Output.csv';
trialType = 'SmoothDiscreteSpeedLineTask';
parseParams = {}
parseParams['minDataPoints'] = 5;  #this is the minimum amount of data points per rep to be counted for this to be counted as a trial!
parseParams['minPercentRecorded'] = .9;
parseParams['maxAvDT'] = .04;
parseParams['skipSinFit'] = 1;

## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType)
#parse Data
[reps,repsParseData]=parseSmoothPData(data,parseParams);

#plot Parse Data
ax1 = plt.figure()
nSpeeds = 4
nReps = 10
#print(reps[2].rawRepData['T'])
for i in range(nSpeeds*nReps):
    plt.subplot(nSpeeds,nReps,i+1)
    line1 = plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['Ob1X'])
    line2 = plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['GazeX'])
    #plt.tight_layout()
plt.suptitle('X axis is Time')

ax2 = plt.figure()
for i in range(nSpeeds*nReps):
    plt.subplot(nSpeeds,nReps,i+1)
    line1 = plt.plot(reps[i].rawRepData['phase'],reps[i].rawRepData['Ob1X'])
    line2 = plt.plot(reps[i].rawRepData['phase'],reps[i].rawRepData['GazeX'])
    #plt.tight_layout()
plt.suptitle('X axis is Phase')


plt.show()

#Display Reps Parse Data
print('All rounds:')
print(repsParseData['allRounds'])
print('Inds:')
print(repsParseData['inds'])

















