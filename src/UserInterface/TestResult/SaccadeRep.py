import matplotlib.lines as linez
import matplotlib.pyplot as plt

class SaccadeRep:


    def __init__(self,rawRepData):
        self.rawRepData = rawRepData
    
    def calcMetric(self,metricName):
        if metricName == 'latency':
            metricVal = self.calcLatency()
        elif metricName == 'directionError':
            metricVal = self.calcDirectionError()
        else:
            print(metricName + ' is not a defined metric for SaccadeRep')
        return metricVal
    
    def plotMetric(self,metricName,ymin,ymax):
        #metricNames = {'latency': self.plotlatency(), 'directionError' : self.plotDirectionError()}
        if metricName == 'latency':
            h = self.plotlatency(ymin,ymax)
        elif metricName == 'directionError':
            h = self.plotDirectionError(ymin,ymax)
        else:
            print(metricName + ' is not a defined metric for SaccadeRep')
        return h
        
    def calcLatency(self):
        rawRepData = self.rawRepData
        #print(rawRepData['type'])
        if (rawRepData['type'].find('antiSaccadeTask')+1):
            d = abs(-rawRepData['ob']-rawRepData['data'])    
        elif (rawRepData['type'].find('saccadeTask')+1):
            d = abs(rawRepData['ob']-rawRepData['data'])
        elif (rawRepData['type'].find('AntiSaccadeTask')+1):
            d = abs(-rawRepData['ob']-rawRepData['data'])
        elif (rawRepData['type'].find('SaccadeTask')+1):
            d = abs(rawRepData['ob']-rawRepData['data'])
        else:
            print('Try using ''saccadeTask'' or ''antiSaccadeTask''!')
            
        section = rawRepData['t'][d<rawRepData['thresh']]
        
        latency = 0
        if len(section) > 0:
            if section[0] != 0:
                latency = section[0]
            else:
                if len(section) > 1:
                    latency = section[1]
                else:
                    latency = NaN
        else:
            latency = NaN
        
        if self.calcDirectionError() == 1:
            latency = NaN
        return latency
    
    def calcDirectionError(self):
        rawRepData = self.rawRepData
        if (rawRepData['type'].find('antiSaccadeTask')+1):
            d = abs(-rawRepData['ob']-rawRepData['data'])    
        elif (rawRepData['type'].find('saccadeTask')+1):
            d = abs(rawRepData['ob']-rawRepData['data'])
        elif (rawRepData['type'].find('AntiSaccadeTask')+1):
            d = abs(-rawRepData['ob']-rawRepData['data'])
        elif (rawRepData['type'].find('SaccadeTask')+1):
            d = abs(rawRepData['ob']-rawRepData['data'])
        else:
            print('Try using ''saccadeTask'' or ''antiSaccadeTask''!')
        
        directionError = 0
        d = d[rawRepData['t']<1]
        if len(rawRepData['ob']) > 0:
            d = d[d>abs(rawRepData['ob'][0])+rawRepData['ethresh']]
            if len(d) > 0:
                directionError = 1
        else:
            directionError = NaN
       
    def plotlatency(self,ymin,ymax):
        latency = self.calcLatency()
        #linez.Line2D([latency,latency],[ymin,ymax],color='r',linewidth='2')
        h= plt.plot([latency,latency],[ymin,ymax],color='r',linewidth = '2')
        return h
        
    def plotDirectionError(self):
        print(2)
