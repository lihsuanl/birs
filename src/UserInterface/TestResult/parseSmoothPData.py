#! python3
#! /usr/bin/python
import sys
import csv
import numpy as np
import math

from SmoothPRep import SmoothPRep

from SmoothPSinFit import SmoothPSinFit

def parseSmoothPData(data,parseParams):
    PI = 3.14159
    
    cycleIndex = data['cycleIndex']
    cycleIndex[np.isnan(cycleIndex)] = 0
    inds = np.unique(cycleIndex)
    roundz = data['round']
    allRounds = np.unique(roundz)
    allRounds = allRounds[allRounds<100] #this line added to remove data for erroneous index labelling
    inds = inds[inds<100] #this line added to remove data for erroneous index labelling
    repsParseData = {}
    i_rep = 0
    repsParseData['inds'] = inds
    repsParseData['allRounds'] = allRounds
    validCount = 0
    Rad = np.max(np.abs(data['Ob1X']))
    reps = np.empty([len(allRounds)*len(inds)],dtype = np.object)
    for i_c in range(0,len(inds)):
        for i_r in range(0,len(allRounds)):
            thisInds = np.where((cycleIndex==inds[i_c]) & (roundz==allRounds[i_r]))[0]
            repData = {}
            repData['valid'] = 1
            repData['T'] = data['T'][thisInds]
            
            repData['Ob1X'] = data['Ob1X'][thisInds]
            repData['GazeX'] = data['GazeX'][thisInds]
            repData['percentRecorded'] = len(np.where(~np.isnan(repData['GazeX']))[0])/len(repData['GazeX'])
            repData['round'] = allRounds[i_r]
            repData['cycle'] = inds[i_c]
            #Here, we will compute the phase from object position
            max_ind = np.where(repData['Ob1X'] == np.max(repData['Ob1X']))[0]
            if len(max_ind) > 1:
                max_ind = max_ind[0]
            
            phase_inc = np.arange(0,max_ind)
            phase_dec = np.arange(max_ind,len(repData['Ob1X']))
            thisPhase_inc = np.arcsin(repData['Ob1X'][phase_inc]/Rad)*180/PI++90
            thisPhase_dec = 270-np.arcsin(repData['Ob1X'][phase_dec]/Rad)*180/PI
            repData['phase'] = np.concatenate([thisPhase_inc,thisPhase_dec])
            #Here, lets add the sinusoidal fitting, THIS TRANSLATION CAN BE SKIPPED FOR NOW
            if parseParams['skipSinFit'] != 1:
                repData['sinFit'] = SmoothPSinFit(repData);
            
            #Here, we will make a set of constraints for this parsed set of data
            if len(thisInds) < parseParams['minDataPoints']:
                repData['valid'] = 0
            
            if repData['percentRecorded'] < parseParams['minPercentRecorded']:
                repData['valid'] = 0
                
            tempRep = SmoothPRep(repData)
            if tempRep.avDT > parseParams['maxAvDT']:
                repData['valid'] = 0
            
            if 'maxUnrecordedInterval' in parseParams:
                if tempRep.maxUnrecordedInterval > parseParams['maxUnrecordedInterval']:
                    repData['valid'] = 0
            
            if repData['valid']:
                validCount += 1
            else:
                invalidCount += 1
            
            reps[i_rep] = SmoothPRep(repData)
            #if i_rep == 2:
            #    print(reps[2].rawRepData['T'])
            i_rep += 1
            

    print('Completed Parseing 1 Trial')
    #if ~exist('reps','var')
        #reps=[];
    #end
    
    return reps,repsParseData






















