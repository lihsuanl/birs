#! /usr/bin/python
import sys
import csv
import numpy as np
import math

from TrapRep import TrapRep

def parseTrapData(data,parseParams):
    cycleIndex = data['cycleIndex']
    cycleIndex[np.isnan(cycleIndex)] = 0 #this counteracts setting everything to zero
    inds = np.unique(cycleIndex)
    roundz = data['round']
    allRounds = np.unique(roundz)
    allRounds = allRounds[allRounds<100] #this line added to remove data for erroneous labelling
    inds = inds[inds<100] #this line added to remove data for erroneous labelling
    i_rep = 0
    repsParseData = {}
    repsParseData['inds'] = inds
    repsParseData['allRounds'] = allRounds
    validCount = 0
    invalidCount = 0
    
    
    reps = np.empty([len(inds)*len(allRounds)],dtype=np.object)
    for i_c in range(0,len(inds)):
        for i_r in range(0,len(allRounds)):
            thisInds = np.where((cycleIndex == inds[i_c]) & (roundz == allRounds[i_r]))[0]
            repData = {}
            repData['valid'] = 1
            repData['T'] = data['T'][thisInds]
            repData['Ob1X'] = data['Ob1X'][thisInds]
            repData['GazeX'] = data['GazeX'][thisInds]
            repData['percentRecorded'] = len(np.where(~np.isnan(repData['GazeX']))[0])/len(repData['GazeX'])
            repData['round'] = allRounds[i_r]
            repData['cycle'] = inds[i_c]
            
            #Here, we will make a set of constraints for this parsed set of data
            if len(thisInds) < parseParams['minDataPoints']:
                repData['valid'] = 0
            if repData['percentRecorded'] < parseParams['minPercentRecorded']:
                repData['valid'] = 0
                
            tempRep = TrapRep(repData)
            if tempRep.avDT > parseParams['maxAvDT']:
                repData['valid'] = 0
            
            if 'maxUnrecordedInterval' in parseParams:
                if tempRep.maxUnrecordedInterval > parseParams['maxUnrecordedInterval']:
                    repData['valid'] = 0
            
            if repData['valid']:
                validCount += 1
            else:
                invalidCount += 1
            
            reps[i_rep] = TrapRep(repData)
            
            i_rep += 1

    repsParseData['validCount'] = validCount
    repsParseData['invalidCount'] = invalidCount
            
    return reps, repsParseData        
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
















