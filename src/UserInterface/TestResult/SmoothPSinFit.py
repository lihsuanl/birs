import numpy as np
import sys
from sinuFit import sinuFit
import matplotlib.pyplot as plt

class SmoothPSinFit:
    
    def __init__(self,rawRepData):
        phase = rawRepData['phase']
        ob = rawRepData['Ob1X']
        gaze = rawRepData['GazeX']
        t = rawRepData['T']
        PhOb = {}
        PhGaze = {}
        try:
            [PhOb['amp'],PhOb['freq'],PhOb['offset'],PhOb['fit'],PhOb['gof']] = sinuFit(phase,ob)
        except:            
            PhOb = sys.exc_info()[0]
        try:
            [PhGaze['amp'],PhGaze['freq'],PhGaze['offset'],PhGaze['fit'],PhGaze['gof']] = sinuFit(phase[~np.isnan(gaze)],gaze[~np.isnan(gaze)])
        except:
            PhGaze = sys.exc_info()[0]
        
        self.PhOb = PhOb
        self.PhGaze = PhGaze
        self.ob = ob
        self.t = t
        self.gaze = gaze
        self.phase = phase
    
    def getPhaseObjEst(self):
        a = self.PhOb['amp']['est']
        b = self.PhOb['freq']['est']
        c = self.PhOb['offset']['est']
        y = a*np.sin(b*self.phase+c);
        #print(self.phase)
        #print(a)
        #print(b)
        #print(c)
        return y
    def getPhaseGazeEst(self):
        a = self.PhGaze['amp']['est']
        b = self.PhGaze['freq']['est']
        c = self.PhGaze['offset']['est']
        y = a*np.sin(b*self.phase+c);
        
        return y
    def val_confirm(self,**kwargs):
        return kwargs
        
    def plotPhaseFit(self,**kwargs):
        cOb = 'b';
        cGaze = [0 ,.6, 0];
        if kwargs != {}:
            linewidthz = self.val_confirm(**kwargs)
            linewidth_num = linewidthz['linewidth']
        else:
            linewidth_num = 2
        #plot object
        #h = []        
        #h[0] = 
        plt.plot(self.phase,self.ob,color=cOb,linewidth=linewidth_num)
        ob_est = self.getPhaseObjEst()
        #print(ob_est)
        #h[1] = 
        plt.plot(self.phase,ob_est,color=cOb,linestyle=':',linewidth=linewidth_num+1)
        #plot gaze
        #h[2] = 
        plt.plot(self.phase,self.gaze,color=cGaze,linewidth=linewidth_num)
        #plt.show()
        gaze_est = self.getPhaseGazeEst()
        plt.plot(self.phase,gaze_est,color=cGaze,linestyle = ':',linewidth=linewidth_num+1)
         
        [x,ob_peak_ind,gaze_peak_ind] =   self.getPhasePeakLag()
        #plt.title('phLag=' + str(x)+' \nrsq='+ str(self.PhGaze['gof']['rsquare'])+'\ntLag='+ str(self.getTimePeakLag()),size=9,multialignment='center')   
        #plt.title(str(round(x,2))+' \n'+ str(round(self.PhGaze['gof']['rsquare'],2))+'\n'+ str(round(self.getTimePeakLag(),2)),size=9,multialignment='center')
        plt.title(str(round(x,2))+' \n'+ str(round(self.PhGaze['gof']['rsquare'],2))+'\n',size=9,multialignment='center')
        #plt.show()
        if ~np.isnan(ob_peak_ind).all():
            ph_peak_ob = np.mean(self.phase[ob_peak_ind])
            ph_peak_gaze = np.mean(self.phase[gaze_peak_ind])
            ax = plt.gca()
            #print(ax.get_ylim())
            plt.plot([ph_peak_ob,ph_peak_ob],ax.get_ylim(),color=cOb,linewidth=linewidth_num,linestyle=':')
            plt.plot([ph_peak_gaze,ph_peak_gaze],ax.get_ylim(),color=cGaze,linewidth=linewidth_num,linestyle=':')
    
    def isGazeFit(self):
        if 'gof' in self.PhGaze:
            isFit = True
        else:
            isFit = False
        #print(isFit)
        return isFit
        
    def getPhasePeakLag(self):
        if self.isGazeFit() == 0:
            x = float('nan')
            ob_peak_ind = float('nan')
            gaze_peak_ind = float('nan')
        else:
            if self.PhGaze['gof']['rsquare'] < 0.5: #if this is a bad fit we return nan
                x = float('nan')
                ob_peak_ind = float('nan')
                gaze_peak_ind = float('nan')    
            else:
                ph = self.phase
                ob_est = self.getPhaseObjEst()
                ob_peak_ind = ob_est==np.max(ob_est)
                phase_obPeak = ph[ob_peak_ind]
                phase_obPeak = np.mean(phase_obPeak) #in case two values are the same!
                gaze_est = self.getPhaseGazeEst();
                gaze_peak_ind = gaze_est==np.max(gaze_est)
                gaze_obPeak = ph[gaze_peak_ind]
                gaze_obPeak = np.mean(gaze_obPeak); #in case two values are the same!
                x = gaze_obPeak-phase_obPeak
                if x < 0:
                    x=float('nan')
                    ob_peak_ind = float('nan')
                    gaze_peak_ind = float('nan')
        return x,ob_peak_ind,gaze_peak_ind
            
            
    def getTimePeakLag(self):
        if self.isGazeFit()==0:
            x = float('nan')
        else:
            t = self.t
            ob_est = self.getPhaseObjEst()
            t_obPeak = t[ob_est==np.max(ob_est)]
            t_obPeak = np.mean(t_obPeak) #in case two values are the same!
            gaze_est = self.getPhaseGazeEst()
            t_gazePeak = t[gaze_est==np.max(gaze_est)]
            t_gazePeak = np.mean(t_gazePeak) #in case two values are the same!
            x = t_gazePeak-t_obPeak            
            
        return x
            
            
