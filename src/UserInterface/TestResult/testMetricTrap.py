#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseTrapData import parseTrapData
from TrapRep import TrapRep
from matplotlib import rc


import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

#rc('text',usetext=True)
#rc('text.latex',preamble='\usepackage{color}')

#Input arguments to create a Trial object that of type DistanceVergenceTask
fileName='SmoothTrapezoidTask269907097313375_Output.csv'
trialType = 'SmoothTrapezoidTask'
parseParams = {}
parseParams['minDataPoints'] = 5  #minimum amount of data points in a rep for it to be counted as valid.
parseParams['minPercentRecorded'] = .9
parseParams['maxAvDT'] = .04
parseParams['skipSinFit'] = 1

## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType)
#parse Data
[reps,repsParseData]=parseTrapData(data,parseParams)

## Plot and Validate
#plot Individual Reps
ax1 = plt.figure()
nSpeeds = 4
nReps = 10
metricName = 'phasePeakLag'
repCount = 0
#reps[0].calcMetric( 'speed' + str(0) +'integralRatioSplit1StopFalling')
#repVal1 =  reps[0].calcMetric('speed' + str(0) +'integralRatioSplit1StopFalling')
#repVal2 =  reps[0].calcMetric('speed' + str(0) +'integralRatioSplit2StopFalling')
#print(repVal1)
#print(repVal2)
gs1 = gridspec.GridSpec(1,2)
for i_sp in range(nSpeeds):
    for i_rep in range(nReps):
        ax2 = plt.subplot(nSpeeds,nReps,repCount+1)
        plt.plot(reps[repCount].rawRepData['T'],reps[repCount].rawRepData['Ob1X'])
        plt.plot(reps[repCount].rawRepData['T'], reps[repCount].rawRepData['GazeX'])
        metricName1 = 'speed' + str(i_sp) +'integralRatioSplit1StopFalling'
        metricName2 = 'speed' + str(i_sp) +'integralRatioSplit2StopFalling' #the only difference from the prsiouvs line is Split2 vs. Split1
        repVal1 =  reps[repCount].calcMetric(metricName1);
        repVal2 =  reps[repCount].calcMetric(metricName2);
        ax2.set_title(str(round(repVal1,2)) + ',\n ' + str(round(repVal2,2)), size = 9)
        ax2.axes.get_xaxis().set_ticks([])
        ax2.axes.get_yaxis().set_ticks([])
        repCount = repCount+1
        plt.tight_layout()
ax1.suptitle('IntegRat')
plt.subplots_adjust(top=0.85)



plt.show()














