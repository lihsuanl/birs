import numpy as np

class TrapRep:


    def __init__(self,rawRepData):
        self.rawRepData = rawRepData
        self.avDT = np.mean(np.diff(rawRepData['T']))
        recordedT = rawRepData['T'][~np.isnan(self.rawRepData['GazeX'])]
        self.maxUnrecordedInterval = np.max(np.diff(recordedT))
        
    def calcMetric(self,metricName):
        #print(metricName)
        #print(metricName[0:5].find('speed'))
        #print(self.rawRepData['valid'])
        if self.rawRepData['valid'] == 0: #if rep is invalid, rep value will be nan!
            metricVal = float('nan')    
        else:
            #if metricName.find('MSE') == 0: %this makes it so MSE0 returns first speed, MSE1 is second speed, MSE3 is 3rd speed, etc.
                    #cycleComp=str2num(metricName(end));
                    #if obj.rawRepData.cycle==cycleComp
                    #    metricVal=obj.calcMeanSquaredError;
                    #else
                    #    metricVal=nan;
                    #end
            if metricName[0:5].find('speed')==0:  #From now on, we can call these speed1, speed0 as a prefix! 
                cycleComp=int(metricName[5])
                #print(cycleComp)
                #print('hithere')
                if self.rawRepData['cycle']==cycleComp:
                        metricVal=self.calcMetric(metricName[6:])
                else:
                        metricVal=float['nan']
            elif metricName.find('integralRatio') != -1:
                metricVal = self.getIntegralRatio(metricName)
                   
                        
            else:
                if metricVal.find('meanSquaredError')==0:
                    metricVal = self.calcMeanSquaredError()
                else:
                    print(metricName + ' is not a defined metric for TrapRep')
        return metricVal
                  
    def getIntegralRatio(self,metricName):
        #Example metricName is speed1integralRatioSplit1StopRising
        #print(metricName)
        if metricName.find('Rising') != -1:
            #print('hi')
            [r1s,r1e,r2s,r2e] = self.calcIntegralRatios(normType='rising')
        elif metricName.find('Falling') != -1:
            #print('hi')
            [r1s,r1e,r2s,r2e] = self.calcIntegralRatios(normType='falling')
        else:
            print('Specify rising or falling normtype!')
            return
        if metricName.find('Split1') != -1:
            rs = r1s
            re = r1e
        elif metricName.find('Split2') != -1:
            rs = r2s
            re = r2e
        else:
            print('Specify Split1 or Split2 in metricName')
            return
        if metricName.find('Stop')!=-1:
            r = rs
        elif metricName.find('End') != -1:
            
            r = re
        else:
            print('Specify Stop or End in metricName')
        return r
       
    def val_confirm(self,**kwargs):
        return kwargs    
    
    def splitData(self,**kwargs):
        if kwargs != {}:
            normTypez = self.val_confirm(**kwargs)['normType']
        else:
            normTypez = 'rising' 
        d = self.rawRepData
        ob = d['Ob1X']
        g = d['GazeX']
        ind_split = np.where(ob==np.max(ob))[0]
        i1stop = ind_split[0]
        ind_split = ind_split[-1]   
        t1 = d['T'][:ind_split]
        t2 = d['T'][ind_split:]
        g1 = g[:ind_split]
        g2 = g[ind_split:]
        ob1 = ob[:ind_split]
        ob2 = ob[ind_split:]
        i2stop = np.where(ob2==np.min(ob2))[0]
        #print(i2stop)
        #print(g1.max())
        if i2stop.any():
            i2stop = i2stop[0]
        #print(g1)
        #print('how are you')
        if normTypez == 'rising':
            g2 = np.max(g2)-g2
            ob2 = np.max(ob2)-ob2
        else:
            g1 = np.max(g1)-g1
            ob1 = np.max(ob1)-ob1
            
        #normalization routine so that min is zero
        ob1 = ob1 - ob1.min()
        ob2 = ob2 - ob2.min()
        g1 = g1-g1.min()
        g2 = g2-g2.min()
        #print(g1)
        return t1, t2, ob1, ob2, g1, g2, i1stop, i2stop
        
    def calcIntegralRatios(self,**kwargs):    
        if kwargs != {}:
            normTypez = self.val_confirm(**kwargs)['normType']
        else:
            normTypez = 'rising'
        [t1,t2,ob1,ob2,g1,g2,i1stop,i2stop] = self.splitData(normType=normTypez)
        #print(ob1)
        #print(g1)
        r1s = self.calcCumDiffRatio(ob1[:i1stop],g1[:i1stop])
        r2s = self.calcCumDiffRatio(ob2[:i2stop],g2[:i2stop])
        r1e = self.calcCumDiffRatio(ob1,g1)
        r2e = self.calcCumDiffRatio(ob2,g2)
        #print(r1s)
        #print(r2s)
        #print(r1e)
        #print(r2e)
        return r1s, r1e, r2s, r2e
        
    def calcCumDiffRatio(self,ob,g):
        ob = ob[~np.isnan(g)]
        g = g[~np.isnan(g)]
        cumdiff = np.sum(ob)-np.sum(g)
        r = cumdiff/np.sum(ob) 
            
        return r
        
        
        
        
        
        
        
        
        
        
        
