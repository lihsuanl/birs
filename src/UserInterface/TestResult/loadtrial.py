#! python3
#! /usr/bin/python

from readTrial import readTrial
from readTrial_json import readTrial_json
import matplotlib.pyplot as plt



def loadTrial(fileName,trialType):
    types = {
        'HorizontalVergence_speed': {'nTargets' : 2, 'extra_col_names' : ['keyPressed'], 'offSetType' : 'firstVal'},
        'saccadeTask': {'nTargets' : 1, 'extra_col_names' : [], 'offSetType' : 'firstVal'},
        'antiSaccadeTask' : {'nTargets' : 1, 'extra_col_names' : [], 'offSetType' : 'firstVal'},
        'SaccadeTask' : {'nTargets' : 2, 'extra_col_names' : [], 'offSetType' : 'firstVal'},
        'AntiSaccadeTask' : {'nTargets' : 2, 'extra_col_names' : [], 'offSetType' : 'firstVal'},
        'smoothDiscreteSpeedLineTask' : {'nTargets' : 1, 'extra_col_names' : ['speed'], 'offSetType' : 'mean'},
        'smoothTrapezoidTask' : {'nTargets' : 1, 'extra_col_names' : ['speed'], 'offSetType' : 'mean'},
        'SmoothTrapezoidTask' : {'nTargets' : 2, 'extra_col_names' : ['systemTime','round','cycleIndex'], 'offSetType' : 'mean'},
        'SmoothDiscreteSpeedLineTask' : {'nTargets' : 2, 'extra_col_names' : ['round','cycleIndex','systemTime'], 'offSetType' : 'mean'},
        'smoothLineTask' : {'nTargets' : 1, 'extra_col_names' : ['speed'], 'offSetType' : 'mean'},
        'VergenceTaskDistance' : {'nTargets' : 2, 'extra_col_names' : ['SystemTime','focusDistance'], 'offSetType' : 'firstVal'},
        'DistanceVergenceTask' : {'nTargets' : 2, 'extra_col_names' : ['SystemTime','focusDistance'], 'offSetType' : 'firstVal'},
    }
    if trialType == 'smoothDiscreteSpeedLineTask' :
        print('Deprecated Class Name and format')
    elif trialType == 'smoothTrapezoidTask':
        print('Deprecated Class Name and format')
    if trialType in types:
        thistype = types[trialType]
    else:
        print(str(trialType) + ' is not of a valid class')
        return
    if fileName.endswith('.csv'):
        trial = readTrial(fileName,thistype['nTargets'],thistype['extra_col_names'],offSetType=thistype['offSetType']);
    elif fileName.endswith('.json'):
        trial = readTrial_json(fileName,thistype['nTargets'],thistype['extra_col_names'],offSetType=thistype['offSetType'])
    else:
        print('Invalid filename, please use csv or json file')
    return trial
















