/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.TestResult;

import Business.Patient.Patient;
import Business.Task.SaccadeTask;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import javax.imageio.ImageIO;

/**
 *
 * @author Eric Dong
 */
public class PythonLoader {
    
    private BufferedImage outputImg;

    public PythonLoader (String script, String[] args) {
        //  Start the script
        outputImg = null;
        try {
            //  Collect all args and run
            ArrayList<String> allArgs = new ArrayList<>(Arrays.asList(args));
            allArgs.add(0, script);
            allArgs.add(0, "python");
            ProcessBuilder pb = new ProcessBuilder(allArgs);
            Process p = pb.start();

            //  Capture script's std output
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s, imgPath = "";
            while ((s = br.readLine()) != null) {
                System.out.println(s);
                if (s.contains("plot=")) {
                    imgPath = s.split("=")[1];
                }
            }

            //  Capture any errors that might have happened
            br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String err = "";
            while ((s = br.readLine()) != null) {
                err += s + "\n";
            }
            if (!err.isEmpty()) {
                System.out.println("Errors:\n" + err);
                return;
            }

            //   Load and display the image
            File file = new File(imgPath);
            outputImg = ImageIO.read(file);
        }
        catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    public BufferedImage getTestResultImg () {
        return this.outputImg;
    }
}
