#! python3
#! /usr/bin/python
import sys
import csv
import numpy as np
import math

from VergenceRep import VergenceRep

def parseVergenceData_fix(data,parseParams):
    
    #repsParseData will return some type of struct that contains information about how the parsing went (e.g. number of trials dropped etc.)
    
    difference = data['LEyeX'] - data['REyeX']
    T = data['T']
    idx = ~np.isnan(difference)
    T = T[idx]
    difference = difference[idx]
    
    crossing_tuple = np.nonzero(np.diff(data['focusDistance']))
    total_crossings = len(crossing_tuple[0])
    
    # Find the start times of each rep and the total number of reps
    #ind = 1
    crossings = [] #set it as list first, append with data and then convert to numpy array
    cross_type = []
    upflag = 1
    downflag = 1
    for ii in range(1,len(difference)-2):
        #Calculate slope
        #Different threshold crossing conditions based on slope
        slope = difference[ii]-difference[ii-1]
        currentTime = T[ii]
        
        lowerTime = currentTime - parseParams['dur']
        lowerInds = np.where((lowerTime <= T) & (T < currentTime))
        lowerInd = lowerInds[0][0]
        
        upperTime = currentTime + parseParams['dur']
        upperInds = np.where((currentTime <=T) & (T<upperTime))
        upperInd = upperInds[0][len(upperInds[0])-1]
        
        past = np.mean(difference[lowerInd:(ii+1)])
        future = np.mean(difference[ii:(upperInd+1)])
    
 
        if (len(lowerInds[0]) > 0) & (len(upperInds[0]) > 0):
            # Need a minimum number of points in the past and future regions
            if ((ii-lowerInd)>parseParams['min_pts']) & ((upperInd-ii)>parseParams['min_pts']): 
                # Detect a threshold crossing
                if slope > 0:
                    if difference[ii] > parseParams['thr']:
                        #Check to see if duration before and after crossing is stable
                            if (past < parseParams['thr']) & (future > parseParams['thr']):
                                if upflag:
                                    crossings.append(T[ii])
                                    cross_type.append('div')
                                    upflag = 0
                                    downflag = 1
                else:
                    if difference[ii] < parseParams['thr']:
                        if (past > parseParams['thr']) & (future < parseParams['thr']):
                            if downflag:
                                crossings.append(T[ii])
                                cross_type.append('con')
                                upflag = 1
                                downflag = 0
                                
        if len(crossings) > 0:
            if (currentTime - crossings[len(crossings)-1]) >= parseParams['refresh']:
                upflag = 1
                downflag = 1            
            
            
    totalReps = len(crossings)
    repsParseData = np.empty([total_crossings,1])
    reps = np.empty([total_crossings],dtype = np.object)
    #print(cross_type)
    #print(total_crossings)
    for i in range(0,total_crossings):  
        rawRepData = {}

                 
        if i < totalReps:
            idx = np.where(((crossings[i]-parseParams['pre'])<=T) & (T<(crossings[i] + parseParams['post'])))
            trial_t = T[idx]
            trial_t -= np.min(trial_t)
            D = difference[idx]

            #Parse data into exponential
            #if i == 5:
            #    print(cross_type[i].find('con'))
            if len(D):
                if cross_type[i].find('con') != -1:
                    rawRepData['type'] = 'con'
                    #if i == 4:
                        #print('hi')
                    #print(rawRepData['type'])
                    #Normalizing rep to be between 0 and 1
                    #First calculate the offset to bring the minimum to near zero in the convergence reps, the minimum value is at the end of the rep
                    offset = np.mean(D[trial_t > trial_t[len(trial_t)-1]-parseParams['tail']])
                    D -= offset    
                    
                    #Next find the amplitude of the movement to normalize
                    #In the convergence reps, the maximum value occurs at the start
                    scale = np.mean(D[trial_t<parseParams['tail']])
                    D = D/scale

                    #Find delay to eye movement initiation for each vergence
                    #Only calculate delay if trial is properly bounded, meaning that its (max-min) must be below parseParams['bound']
                    if np.max(D)-np.min(D) < parseParams['bound']:
                        #Grab the times before which the eye movement is considered started using parseParams['thresh'] to determine whe eye movement is considered started
                        t_tmp = trial_t[(1-D)<parseParams['thresh']]
                        #Make sure that t_tmp is non-zero
                        if len(t_tmp) > 0:
                            #The last value of t_tmp is the time at which the eye movement is considered initiated
                            t_tmp = t_tmp[len(t_tmp)-1]
                            #Find the index at which movement is initiated
                            delay = np.where(trial_t==t_tmp)
                            D = D[delay[0][0]:(len(D)-1)]
                            trial_t = trial_t[delay[0][0]:(len(trial_t)-1)]-trial_t[delay[0][0]]
                            #Set variable to indiciate that the rep was good
                            repsParseData[i] = 1
                        else:
                            #Set variable to indicate that rep was bad if there was no eye position that was below the threshold
                            repsParseData[i] = -1
                            
                    else:
                        #Set variable to indicate that rep was bad if the rep was not properly bounded
                        repsParseData[i] = 0
                        
                    rawRepData['data'] = D
                else:
                    rawRepData['type'] = 'div'
                    #Subtracting the offset
                    #In the divergence rep, the minimum occurs at the beginning
                    offset = np.mean(D[trial_t<parseParams['tail']])
                    D = D - offset
                    
                    #Normalizing the rep
                    #In the divergence rep, the maximum occurs at the end
                    scale = np.mean(D[trial_t>(trial_t[-1]-parseParams['tail'])])
                    D = D/scale
                    
                    #Find delay to eye movement initiation for each vergence
                    #Only calculate delay if trial is properly bounded, meaning that its (max-min) must be below parseParams['bound']
                    if np.max(D)-np.min(D) < parseParams['bound']:
                        t_tmp = trial_t[D<parseParams['thresh']]
                        if len(t_tmp)>0:
                            t_tmp = t_tmp[-1]
                            delay_array = np.where(trial_t==t_tmp)
                            delay = delay_array[0][0]
                            D = D[delay:(len(D)-1)]
                            trial_t = trial_t[delay:(len(trial_t)-1)]-trial_t[delay]
                            repsParseData[i] = 1
                        else:
                            repsParseData[i] = -1
                    else:
                        repsParseData[i] = 0
                        
                    #Have to flip the trial to make it a decaying exponential
                    rawRepData['data'] = 1-D
                
                rawRepData['t'] = trial_t
            else:
                rawRepData['data'] = D
                rawRepData['t'] = trial_t
                rawRepData['type'] = 'none'
                repsParseData[i] = 0
                
            #Reps that contain only 2 data points or less are invalid
            #Make sure this becomes a parseParam
            
            #If the rep is not long enough, then it is invalid
            if len(trial_t)>0:
                if trial_t[-1] < 0.29:
                    repsParseData[i] = 0
                    
            
            #Catching instances where long chunks of data are dropped within the first 0.7 seconds of the rep where the saccade should occur
            max_dt = np.max(np.diff(trial_t[trial_t<0.7]))
            if max_dt > 0.3:
                repsParseData[i] = 0
                
        else:
            rawRepData['data'] = np.empty((50,1))
            rawRepData['type'] = 0
            rawRepData['t'] = np.arange(1,51)
            repsParseData[i] = 0
        
        
        reps[i] = VergenceRep(rawRepData,repsParseData[i])
        #print(reps[i].rawRepData)
        #if i == 2:
        #    print(reps[i].rawRepData['data'])
        #    print(reps[i].rawRepData['t'])
            
    #print(reps[2].rawRepData['data'])
    return reps, repsParseData            
            
            
            
            
            
    
    
    
    
    
    
    
    
    
