#! python3
#! /usr/bin/python
import sys
import csv
import numpy as np

from SaccadeRep import SaccadeRep

def parseSaccadeData(data,parseParams,typez):
    t = data['T']
    xEye = data['GazeX']
    xOb = data['Ob1X']
    diff_xOb = np.transpose(np.diff(xOb))
    diff_xOb2 = np.array([0])
    dfocus = np.concatenate((diff_xOb2,diff_xOb))
    idx = np.nonzero(dfocus)
    totalReps = len(idx[0])-1
    min_L = 0
    for i in range(len(t)):
        if t[i] > parseParams['window']:
            min_L = i-1
            break
    #print(totalReps)
    ind = 0
    reps = np.empty([int(totalReps/2)+1],dtype = np.object)
    repsParseData = np.empty([int(totalReps/2)+1])
    
    for i in range(0,totalReps,2):
        start_idx = idx[0][i]
        end_idx = idx[0][i] + min_L
        
        #when end_idx is greater than the length of xEye
        if end_idx > len(xEye):
            end_idx = end_ix - (end_idx-len(xEye))
        
        rep_gaze = xEye[start_idx:end_idx]
        rep_xOb = xOb[start_idx:end_idx]
        
        #Get time for rep
        trial_t = t[start_idx:end_idx] - t[start_idx]
        
        
        #remove NaNs from data
        Dinds = ~np.isnan(rep_gaze)
        rep_gaze = rep_gaze[Dinds]
        trial_t = trial_t[Dinds]
        rep_xOb = rep_xOb[Dinds]
        
        #remove offset if zero gaze is not at zero
        offset = np.mean(rep_gaze[np.where(trial_t < 0.1)])
        rep_gaze = rep_gaze - offset
        
        rawRepData = {}
        #store whether it was a left or right trial
        movement = xOb[end_idx] - xOb[start_idx]
        if movement > 0:
            rawRepData['movement'] = 'right'
        else:
            rawRepData['movement'] = 'left'
            
        rawRepData['type'] = typez
        rawRepData['ob'] = rep_xOb
        rawRepData['data'] = rep_gaze
        rawRepData['t'] = trial_t
        rawRepData['thresh'] = parseParams['thresh']
        rawRepData['ethresh'] = parseParams['ethresh']
        
        #SaccadeRep(rawRepData)
        reps[ind] = SaccadeRep(rawRepData)
        repsParseData[ind] = 1
        
        #catching instances where the person forgot to saccade
        if abs(np.mean(rep_gaze)) < 2:
            repsParseData[ind] = 0
            
        #catching instances where the rep is all NaN
        check = np.isnan(rep_gaze)
        if sum(check) == len(check):
            repsParseData[ind] = 0
        
        #catching instancse where long chunks of data are dropped within first 0.7 seconds of the rep where the saccade would occur
        max_dt = max(np.diff(trial_t[np.where(abs(trial_t-0.4) < 0.3)]))
        if max_dt > 0.3:
            repsParseData[ind] = 0
        
        ind += 1
        
        
    return reps, repsParseData
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        