#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math
import os

from readTrial import readTrial
from loadtrial import loadTrial
from parseVergenceData import parseVergenceData_fix
from VergenceRep import VergenceRep

import matplotlib.pyplot as plt

def imgprintVergence(fileName,trialType,output_dir):
    # Vergence params for rep parsing
    # Set threshold to separate convergene and divergence level
    parseParams = {}
    parseParams['thr'] = 7.5
    # Calculate mean eye distance before and after threshold crossing to
    # determine whether an actual convergence or divergence was being
    # maintained
    parseParams['dur'] = 0.5
    # Setting the minimum number of points necessary regions before and after
    # the threshold crossing required for an eye movement to be valid
    parseParams['min_pts'] = 5
    # If a convergence/divergence is missed due to recording error, set a
    # time after which the convergence/divergence flags will be reset
    parseParams['refresh'] = 5
    # Amount of time to grab before detected crossing
    parseParams['pre'] = 0.5
    # Amount of time to grab after detected crossing
    parseParams['post'] = 1
    # Parse params for tau metric calculation
    # Window specifies the length (in seconds) of each rep that
    # is parsed out and so every rep is the same length.
    parseParams['window'] = 3
    # Tail specifies the amount of time (in seconds) at the end or
    # beginning of the rep (depending on whether it is a divergence or
    # convergence trial) that is used to calculate the amplitude of the
    # movement in order to normalize the trial between zero and one
    parseParams['tail'] = 0.2
    # Bound is used to throw out bad trials. After normalization,
    # if the (max-min) of the rep is greater than the bound, the rep is
    # thrown out
    parseParams['bound'] = 4
    # Thresh specifies the percentage from the initial eye position at which
    # the eye is considered to have started moving.
    parseParams['thresh'] = 0.1
    # Need a longer window to calculate the 10-90 latency
    parseParams['Lwindow'] = 2


    ## Intermediate Steps
    #load trial Data
    data = loadTrial(fileName,trialType)
    #parse Data
    [reps,repsParseData]=parseVergenceData_fix(data,parseParams);

    ## Plot and Validate
    #plot Individual Reps

    nPl = len(reps)
    dimPl = math.ceil(math.sqrt(nPl))
    metricNames = ['con-tau','div-tau'];
    ax1 = plt.figure()
    for i in range(nPl):
        for j in range(len(metricNames)):    
            ax2 = plt.subplot(dimPl,dimPl*2,i*2+j+1)
            plt.plot(reps[i].rawRepData['t'],reps[i].rawRepData['data']);
            i_m = metricNames[j]
            reps[i].plotMetric(i_m)
            repVal =  reps[i].calcMetric(i_m);
            #print(str(repVal)+ ' hi again')
            #titleStr = str(i_m) +  '=' + str(repVal) + ', parseData=' +  str(repsParseData[i])
            plt.title(str(i_m)+ '\n' + str(round(repVal,2)) + '\n' + str(repsParseData[i]), size=9)
            ax2.axes.get_xaxis().set_ticks([])
            ax2.axes.get_yaxis().set_ticks([])
            plt.tight_layout()
    ax1.suptitle('parseData')  
    plt.subplots_adjust(top=0.85) 
    imgname = fileName[:-4] + 'image.png'
    savedname = os.path.join(str(output_dir) + imgname)
    print(savedname)
    ax1.savefig(savedname)
    return 'plot=' + savedname
   
