#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseSaccadeData import parseSaccadeData
from SaccadeRep import SaccadeRep

import matplotlib.pyplot as plt


#Input arguments to create a Trial object that of type SaccadeTask
#fileName='SaccadeTask270234174606797_Output.csv';
#fileName = 'SaccadeTask721048223002857_Output.csv'
fileName = 'SaccadeTask721126024969547.json'
trialType = 'SaccadeTask';
#### Window specifies the length (in seconds) of each rep that 
#### is parsed out and so every rep is the same length.
parseParams = {}
parseParams['window'] = 2.0;
#### Thresh specifies the error (in degrees) that is used by the latency
# metric to determine whether
parseParams['thresh'] = 5.0;
parseParams['ethresh'] = 5.0;


## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType);


#parse Data
[reps,repsParseData]=parseSaccadeData(data,parseParams,trialType,'json');
print(reps[19].rawRepData)
#plot Parse Data
ax1 = plt.figure()
nPl = len(reps);
dimPl = math.ceil(math.sqrt(nPl));
for i in range(nPl):
    ax2 = plt.subplot(dimPl,dimPl,i+1)
    print(i)
    plt.plot(reps[i].rawRepData['t'],reps[i].rawRepData['data']);
    ymin,ymax = ax2.get_ylim()
    h = reps[i].plotMetric('latency',ymin,ymax)
    #latency = reps[i].calcLatency()
    #plt.plot([latency,latency],[ymin,ymax],color ='r',linewidth=2)
    repVal =  reps[i].calcMetric('latency')
    #titleStr = 'latency=' + str(repVal) + ', parseData=' + str(repsParseData[i])
    ax2.set_ylim([-20, 20])
    ax2.axes.get_xaxis().set_ticks([])
    ax2.axes.get_yaxis().set_ticks([])
    plt.title(str(repVal) + '\n' + str(repsParseData[i]))
    plt.tight_layout()

ax1.suptitle('latency, parseData')
plt.subplots_adjust(top=0.85)
plt.show()


















