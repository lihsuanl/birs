#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseSmoothPData import parseSmoothPData
from SmoothPRep import SmoothPRep
from SmoothPSinFit import SmoothPSinFit
#from sinuFit import sinuFit

import matplotlib.pyplot as plt



#Input arguments to create a Trial object that of type DistanceVergenceTask
fileName='SmoothDiscreteSpeedLineTask269808029650582_Output.csv'
trialType = 'SmoothDiscreteSpeedLineTask'
parseParams = {}
parseParams['minDataPoints'] = 5  #this is the minimum amount of data points per rep to be counted for this to be counted as a trial!
parseParams['minPercentRecorded'] = .9
parseParams['maxAvDT'] = .04
parseParams['skipSinFit'] = 0

## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType);
#parse Data
[reps,repsParseData]=parseSmoothPData(data,parseParams);

## Validate the fit of an idividual rep
ax1 = plt.figure()
reps[1].rawRepData['sinFit'].plotPhaseFit()
#ax1.suptitle('phLag rsq tlag')
#ax1.suptitle.set_position([0.5, 1.05])
plt.tight_layout()

## Plot and Validate calcMetric
ax2 = plt.figure()
nSpeeds = 4
nReps = 10
metricName = 'phasePeakLag'
for i in range(nSpeeds*nReps):
    ax = plt.subplot(nSpeeds,nReps,i+1)
    plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['Ob1X'])
    plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['GazeX'])
    repVal =  reps[i].calcMetric(metricName)
    plt.title(str(round(repVal,2)),size=9)
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.tight_layout()
    #title(titleStr,'FontSize',9)
ax2.suptitle(metricName)
plt.subplots_adjust(top=0.85)

## plot Fitting Results of all Reps
ax3 = plt.figure()
nSpeeds = 4
nReps = 10
ax3.suptitle('phLag rsq tlag')
for i in range(nSpeeds*nReps):
    ax = plt.subplot(nSpeeds,nReps,i+1)
    reps[i].rawRepData['sinFit'].plotPhaseFit()
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.tight_layout()
plt.subplots_adjust(top=0.85)

plt.show()















