#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseSaccadeData import parseSaccadeData

import matplotlib.pyplot as plt

#clearvars

#Input arguments to create a Trial object that of type SaccadeTask
fileName='SaccadeTask270234174606797_Output.csv';
trialType = 'SaccadeTask';
#### Window specifies the length (in seconds) of each rep that 
#### is parsed out and so every rep is the same length.
parseParams = {}
parseParams['window'] = 2.0;
#### Thresh specifies the error (in degrees) that is used by the latency
# metric to determine whether
parseParams['thresh'] = 5.0;
parseParams['ethresh'] = 5.0;


## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType);
#parse Data
[reps,repsParseData]= parseSaccadeData(data,parseParams,trialType);

#plot Parse Data
ax1 = plt.figure()
nPl = len(reps)
dimPl = math.ceil(math.sqrt(nPl))
for i in range(nPl):
    plt.subplot(dimPl,dimPl,i+1)
    plt.plot(reps[i].rawRepData['t'],reps[i].rawRepData['data'])

#plot repsParseData
ax2 = plt.figure()
plt.plot(repsParseData,'o')
plt.xlabel('Rep')
plt.ylabel('Parse = 1, No Parse = 0')

plt.show()















