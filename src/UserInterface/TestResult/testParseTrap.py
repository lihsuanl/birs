#! python3
#! /usr/bin/python
import sys
import csv
import numpy
import math

from readTrial import readTrial
from loadtrial import loadTrial
from parseTrapData import parseTrapData 

import matplotlib.pyplot as plt

#Input arguments to create a Trial object that of type DistanceVergenceTask
fileName='SmoothTrapezoidTask269907097313375_Output.csv'
trialType = 'SmoothTrapezoidTask'
parseParams = {}
parseParams['minDataPoints'] = 5  #minimum amount of data points in a rep for it to be counted as valid.
parseParams['minPercentRecorded'] = .9
parseParams['maxAvDT'] = .04
parseParams['skipSinFit'] = 1

## Intermediate Steps
#load trial Data
data = loadTrial(fileName,trialType)
#parse Data
[reps,repsParseData]=parseTrapData(data,parseParams)

#plot Parse Data
ax1 = plt.figure()
nSpeeds = 4
nReps = 10
#print(reps[2].rawRepData['T'])
for i in range(nSpeeds*nReps):
    plt.subplot(nSpeeds,nReps,i+1)
    line1 = plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['Ob1X'])
    line2 = plt.plot(reps[i].rawRepData['T'],reps[i].rawRepData['GazeX'])

##Display Reps Parse Data
print('All rounds:')
print(repsParseData['allRounds'])
print('Inds:')
print(repsParseData['inds'])
print('Valid Count:')
print(repsParseData['validCount'])
print(repsParseData['invalidCount'])



plt.show()





