#! python3
#! /usr/bin/python

from readTrial import readTrial
import matplotlib.pyplot as plt



# 1) test readTrial function
nTargets=2;
extra_col_names=[]; #(there are no extra column names for this task)
f = 'SaccadeTask270234174606797_Output.csv';
trial = readTrial(f,nTargets,extra_col_names);

## 2) Look at results to validate all data was read correctly 
fig = plt.figure()
ax1 = plt.subplot("211")
line1 = ax1.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = ax1.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = ax1.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = ax1.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = ax1.plot(trial['T'],trial['REyeX'], label = 'REye')
ax1.set_title('X Position')
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Position (degrees)')
plt.legend()

ax2 = plt.subplot("212")
line1 = ax2.plot(trial['T'],trial['Ob1Y'])
line2 = ax2.plot(trial['T'],trial['Ob2Y'])
line3 = ax2.plot(trial['T'],trial['GazeY'])
line4 = ax2.plot(trial['T'],trial['LEyeY'])
line5 = ax2.plot(trial['T'],trial['REyeY'])
ax2.set_title('Y Position')
ax2.set_xlabel('Time (s)')
ax2.set_ylabel('Position (degrees)')

plt.show()










































