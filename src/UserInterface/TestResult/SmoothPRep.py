import numpy as np
from SmoothPSinFit import SmoothPSinFit

class SmoothPRep:


    def __init__(self,rawRepData):
        self.rawRepData = rawRepData
        self.avDT = np.mean(np.diff(rawRepData['T']))
        recordedT = rawRepData['T'][~np.isnan(self.rawRepData['GazeX'])]
        self.maxUnrecordedInterval = np.max(np.diff(recordedT))
    
    def calcMetric(self,metricName):
        if self.rawRepData['valid'] == 0:
            metricVal = float('nan')
        else:
            if metricName == 'meanSquaredError':
                metricVal = self.calcMeanSquaredError()
            elif metricName == 'phasePeakLag':
                metricVal = self.rawRepData['sinFit'].getPhasePeakLag()
            elif metricName == 'timePeakLag':
                metricVal = self.rawRepData['sinFit'].getTimePeakLag()
            else:
                print(metricName + ' is not a defined metric for SmoothPRep')
        return metricVal[0]
    def val_confirm(self,**kwargs):
        return kwargs

    def plotRepsvsPhase(self,**kwargs):
        line_options = val_confirm(**kwargs,color='b',plGaze=1)
        p = self.rawRepData['phase']
        plt.plot(p,self.rawRepData['Ob1X'],linestyle='none',marker='.',markerfacecolor='k',markeredgecolor='k')
        if plGaze:
            h = plt.plot(p,self.rawRepData['GazeX'],color=line_options['color'])
            
                
        
        
        
        
        
        
        
        
        
        
        
        
