import numpy as np
import sys
import matplotlib.pyplot as plt
import glob, os

from imgprintSaccade import imgprintSaccade
from imgprintVergence import imgprintVergence
from imgprintTrapezoid import imgprintTrapezoid
from imgprintSmoothP import imgprintSmoothP

def javapython_bridge(input_dir, tasktype,output_dir):
    os.chdir(input_dir)
    tasktypez = tasktype + '*'
    types = ('*.csv', tasktypez)
    files_grabbed = []
    if tasktype == 'SaccadeTask':
        for files in glob.glob(tasktypez):
            #print(files)
            if files.endswith('.csv'):
                img = imgprintSaccade(files,tasktype, output_dir)
    elif tasktype == 'DistanceVergenceTask':
        for files in glob.glob(tasktypez):
            #print(files)
            if files.endswith('.csv'):
                img = imgprintVergence(files,tasktype, output_dir)
    elif tasktype == 'SmoothTrapezoidTask':
        for files in glob.glob(tasktypez):
            #print(files)
            if files.endswith('.csv'):
                img = imgprintTrapezoid(files,tasktype, output_dir)
    elif tasktype == 'SmoothDiscreteSpeedLineTask':
        for files in glob.glob(tasktypez):
            #print(files)
            if files.endswith('.csv'):
                img = imgprintSmoothP(files,tasktype, output_dir)
    else:
        print('tasktype not recognized, please try again')
        return

    
    #print(img)
    return img








javapython_bridge('C:\\Users\\lihsuanl\\Documents\\EyeTracking','SaccadeTask','C:\\Users\\lihsuanl\\Documents\\EyeTracking\\SavedImages\\')
#javapython_bridge('blah2','blah1')

