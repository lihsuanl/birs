#! python3
#! /usr/bin/python
import sys
import csv
import numpy as np
import math
from scipy.optimize import curve_fit
from scipy.stats import distributions
import matplotlib.pyplot as plt


def func(x,p1,p2,p3):
    return p1*np.sin(p2*x+p3)

def sinuFit(X,Y):
    #print('hi')
    pars, pcov = curve_fit(func,X,Y,p0=[3*np.std(Y)/(2**0.5),2*np.pi/(np.max(X)-np.min(X)),1])
    gof = {}
    y_mean = np.ones(len(Y))*np.mean(Y)
    SSR = np.sum((func(X,pars[0],pars[1],pars[2])-y_mean)**2)
    SST = np.sum((Y-y_mean)**2)
    gof['rsquare'] = SSR/SST
    gof['rsme'] = np.sqrt(np.sum((func(X,pars[0],pars[1],pars[2])-Y)**2)/len(Y))
    
    amp = {}
    freq = {}
    offset = {}
    amp['est'] = pars[0]
    freq['est'] = pars[1]
    offset['est'] = pars[2]
    #print(pars)
    #print(gof['rsquare'])
    #Calculate confidence intervals
    alpha = 0.05
    n = len(Y)
    p = len(pars)
    dof = max(0,n-p)
    tval = distributions.t.ppf(1-alpha/2,dof)
    
    amp['Z'] = pcov[0][0]*tval/1.96
    freq['Z'] = pcov[1][1]*tval/1.96
    offset['Z'] = pcov[2][2]*tval/1.96
    return amp,freq,offset,pars,gof
    
    
    
    
    
    

