#! python3
#! /usr/bin/python
import sys
import math
import numpy

#constants
pi = 3.14159

from func1 import process_options
    
def pix2degrees(pix,dpl,distance):
    m,n = pix.shape
    deg = numpy.ndarray(shape = pix.shape)

    
    for j in range(n):
        for i in range(m):
            #print(len(pix[i]))
            w = pix[i,j]/dpl
            #print(len(w))
            rad = numpy.arctan2(w,distance)
            deg[i,j] = rad*180/pi
    return deg
