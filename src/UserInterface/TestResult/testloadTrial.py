#! python3
#! /usr/bin/python

from loadtrial import loadTrial
import matplotlib.pyplot as plt












#some code
## This is the main script that validates loadTrial
#There are 5 main trial types that we want to load for now:

# 'SaccadeTask'
# 'AntiSaccadeTask'
# 'DistanceVergenceTask'
# 'SmoothDiscreteSpeedLineTask'
# 'SmoothTrapezoidTask'

## 1 Saccade Task
#load task
f = 'SaccadeTask270234174606797_Output.csv';
trialType = 'SaccadeTask';
trial = loadTrial(f,trialType);
# look at results to validate data is read correctly
ax1 = plt.figure()
line1 = plt.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = plt.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = plt.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = plt.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = plt.plot(trial['T'],trial['REyeX'], label = 'REye')
plt.title('X Position ' + f)
plt.xlabel('Time (s)')
plt.ylabel('Position (degrees)')
plt.legend()
#ax1.show()

## 2 AntiSaccade Task
#load task
f = 'AntiSaccadeTask270336271200482_Output.csv';
trialType = 'AntiSaccadeTask';
trial = loadTrial(f,trialType);
# look at results to validate data is read correctly
ax2 = plt.figure()
line1 = plt.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = plt.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = plt.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = plt.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = plt.plot(trial['T'],trial['REyeX'], label = 'REye')
plt.title('X Position ' + f)
plt.xlabel('Time (s)')
plt.ylabel('Position (degrees)')
plt.legend()
#ax2.show()

## 3 DistanceVergenceTask
#load task
f = 'DistanceVergenceTask270059651001941_Output.csv';
trialType = 'DistanceVergenceTask';
trial = loadTrial(f,trialType);
# look at results to validate data is read correctly
ax3 = plt.figure()
line1 = plt.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = plt.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = plt.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = plt.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = plt.plot(trial['T'],trial['REyeX'], label = 'REye')
line6 = plt.plot(trial['T'],trial['focusDistance'], label = 'FocusDistance')
plt.title('X Position ' + f)
plt.xlabel('Time (s)')
plt.ylabel('Position (degrees)')
plt.legend()

## 4 SmoothDiscreteSpeedLineTask
#load task
f = 'SmoothDiscreteSpeedLineTask269808029650582_Output.csv';
trialType = 'SmoothDiscreteSpeedLineTask';
trial = loadTrial(f,trialType);
# look at results to validate data is read correctly
ax4 = plt.figure()
line1 = plt.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = plt.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = plt.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = plt.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = plt.plot(trial['T'],trial['REyeX'], label = 'REye')
line6 = plt.plot(trial['T'],trial['round'], label = 'Round')
line7 = plt.plot(trial['T'],trial['cycleIndex'], label = 'CycleIndex')
plt.title('X Position ' + f)
plt.xlabel('Time (s)')
plt.ylabel('Position (degrees)')
plt.ylim(-20,20)
plt.legend()


## 5 SmoothTrapezoidTask
#load task
f = 'SmoothTrapezoidTask269907097313375_Output.csv';
trialType = 'SmoothTrapezoidTask';
trial = loadTrial(f,trialType);
# look at results to validate data is read correctly
ax5 = plt.figure()
line1 = plt.plot(trial['T'],trial['Ob1X'],label = 'Ob1')
line2 = plt.plot(trial['T'],trial['Ob2X'],label = 'Ob2')
line3 = plt.plot(trial['T'],trial['GazeX'],label = 'Gaze')
line4 = plt.plot(trial['T'],trial['LEyeX'], label = 'LEye')
line5 = plt.plot(trial['T'],trial['REyeX'], label = 'REye')
line6 = plt.plot(trial['T'],trial['round'], label = 'Round')
line7 = plt.plot(trial['T'],trial['cycleIndex'], label = 'CycleIndex')
plt.title('X Position ' + f)
plt.xlabel('Time (s)')
plt.ylabel('Position (degrees)')
plt.ylim(-20,20)
plt.legend()
















plt.show()

