/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Test;

import Business.Test.TestData;

/**
 *
 * @author neethan
 */
public class TestResult {

    private TestData originalData;

    public TestResult() {

    }

    public TestResult(TestData originalData) {
        this.originalData = originalData;
    }

    public TestData getOriginalData() {
        return originalData;
    }

    public void setOriginalData(TestData originalData) {
        this.originalData = originalData;
    }

}
