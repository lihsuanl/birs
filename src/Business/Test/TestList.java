/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author neethan
 */
public class TestList {

    private ArrayList<Test> testList;
    private String dateOfTest;

    public TestList() {
        testList = new ArrayList<>();
        dateOfTest = getCurrentDate();
    }

    // The fuction is used to get the current date with time
    private String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public ArrayList<Test> getTestList() {
        return testList;
    }

    public void setTestList(ArrayList<Test> testList) {
        this.testList = testList;
    }

    public String getDateOfTest() {
        return dateOfTest;
    }

    public void setDateOfTest(String dateOfTest) {
        this.dateOfTest = dateOfTest;
    }

    // The function is used to create a new test and add it to the list
    public Test createAndAddTest() {
        Test test = new Test();
        testList.add(test);
        return test;
    }

    // The function is used to add an existed test to the list
    public void addTest(Test test) {
        testList.add(test);
    }

    // The function is used to remove an existed test from the list
    public void removeTest(Test test) {
        testList.remove(test);
    }

}
