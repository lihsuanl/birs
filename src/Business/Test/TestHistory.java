/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Test;

import java.util.ArrayList;

/**
 *
 * @author neethan
 */
public class TestHistory {

    private ArrayList<TestList> testHistory;

    public TestHistory() {
        testHistory = new ArrayList<>();
    }

    public ArrayList<TestList> getTestHistory() {
        return testHistory;
    }

    public void setTestHistory(ArrayList<TestList> testHistory) {
        this.testHistory = testHistory;
    }

    // The function is used to create a new testList and add it to the history
    public TestList createAndAddList() {
        TestList testList = new TestList();
        testHistory.add(testList);
        return testList;
    }

    // The function is used to add an existed test list to the history
    public void addList(TestList testList) {
        testHistory.add(testList);
    }

    // The function is used to remove an existed test list from the history
    public boolean removeList(TestList testList) {
        if (testHistory.remove(testList)) {
            return true;
        };
        return false;
    }

}
