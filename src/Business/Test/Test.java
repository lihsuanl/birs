/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Test;

import Business.Task.Task;

/**
 *
 * @author neethan
 */
public class Test {

    private Task task;
    private TestData data;
    private TestResult result;
    private int testId;
    private static int counter = 1;

    public Test() {
        data = new TestData();
        result = new TestResult();
        testId = counter;
        counter++;
    }

    public Test(Task task) {
        data = new TestData();
        result = new TestResult();
        testId = counter;
        counter++;
        this.task = task;
    }
    
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public TestData getData() {
        return data;
    }

    public void setData(TestData data) {
        this.data = data;
    }

    public TestResult getResult() {
        return result;
    }

    public void setResult(TestResult result) {
        this.result = result;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

}
