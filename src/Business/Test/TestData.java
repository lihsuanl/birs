/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Test;

import Business.Task.Task;
import com.theeyetribe.client.data.Point2D;

/**
 *
 * @author neethan
 */
public class TestData {

    private Task task;
    private Point2D[] targetLocs;
    private Point2D[] GazeLocs;

    public TestData() {
    }

    public TestData(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

}
