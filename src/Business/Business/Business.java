/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Business;

import Business.Patient.PatientDirectory;

/**
 *
 * @author neethan
 */
public class Business {

    private static Business business;
    private PatientDirectory patientList;

    private Business() {
        if (business != null) {
            throw new IllegalStateException("Already instantiated");
            //return;
        }
        patientList = new PatientDirectory();
    }

    public static Business getInstance() {
        if (business == null) {
            business = new Business();
        }
        return business;
    }

    public PatientDirectory getPatientList() {
        return patientList;
    }

    public void setPatientList(PatientDirectory patientList) {
        this.patientList = patientList;
    }

}
