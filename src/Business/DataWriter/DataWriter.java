/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DataWriter;

import Business.Patient.Patient;
import Business.Task.Task;
import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.data.CalibrationResult;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 *
 * @author neethan
 */
public class DataWriter {

    private String fileURL;
    private FileWriter fileWriter;
    private FileWriter calibrationWriter;
    private GazeManager gm;
    private long createDate;
    private String firstline;
    private Task task;
    private String userName;
    private ArrayList<String> gazeData;

    public DataWriter(GazeManager gm, Task task, Patient patient) {
        this.gm = gm;
        createDate = System.nanoTime();
        this.task = task;
        this.initiateFile(task, patient);
        gazeData = new ArrayList<String>();
    }

    public DataWriter(Patient patient) {
        createDate = System.nanoTime();
        this.initiateFile(patient);
        gazeData = new ArrayList<String>();
    }

    private void initiateFile(Patient patient) {
        fileURL = patient.getOutputFolder();
        userName = System.getProperty("user.name");
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.indexOf("mac") >= 0) {
            String folderName = patient.getPatientId() + patient.getName();
            fileURL = fileURL + "/" + folderName + "/";
        } else if (osName.indexOf("win") >= 0) {
            String folderName = patient.getName() + patient.getPatientId();
            fileURL = fileURL + "\\" + folderName + "\\";
        }

        boolean success = (new File(fileURL).mkdirs());
        if (!success) {
            System.out.println("File create failed.");
        }
        fileURL += String.valueOf(createDate);

        File file = new File(fileURL + "_TestNote.txt");
        try {
            file.createNewFile();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            fileWriter = new FileWriter(file);
            System.out.println("A New File Writer has been created!");
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    private void initiateFile(Task task, Patient patient) {
        //Change the fileURL here base on what platform you are running the system.
        fileURL = patient.getOutputFolder();
        userName = System.getProperty("user.name");
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.indexOf("mac") >= 0) {
            String folderName = patient.getPatientId() + patient.getName();
            fileURL = fileURL + "/" + folderName + "/";
        } else if (osName.indexOf("win") >= 0) {
            String folderName = patient.getName() + patient.getPatientId();
            fileURL = fileURL + "\\" + folderName + "\\";
        }

        boolean success = (new File(fileURL).mkdirs());
        if (!success) {
            System.out.println("File create failed.");
        }
        String fileName = task.getType() + String.valueOf(createDate);
        fileURL = fileURL + fileName;

        double screenDistance = task.getScreenDistance();
        getCalibration(fileURL + "_Calibration.csv", screenDistance);

        File file = new File(fileURL + "_Output.csv");
        try {
            file.createNewFile();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            fileWriter = new FileWriter(file);
            //System.out.println("A New File Writer has been created!");
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String paramLine = "screenDistance=," + task.getScreenDistance() + ", in.";

        if (task.getNumOfTarget() == 1 || task.getNumOfTarget() == 2) {
            firstline = "Object1  X, Object1 Y, Object2  X, Object2 Y, Raw Gaze Eye X, Raw Gaze Eye Y, Left Eye X, Left Eye Y, Right Eye X, Right Eye Y, Time Since " + createDate + "(ms), System Time";
        } else if (task.getNumOfTarget() == 0) {
            firstline = "Raw Gaze Eye X, Raw Gaze Eye Y, Left Eye X, Left Eye Y, Right Eye X, Right Eye Y, Time Since " + createDate + "(ms), System Time";
        }

        try {
            Enumeration taskParamsKey = task.getTaskParams().keys();
            while (taskParamsKey.hasMoreElements()) {
                String paramsKey = (String) taskParamsKey.nextElement();
                fileWriter.write(paramsKey + ", " + task.getTaskParams().get(paramsKey) + "\n");
            }
            fileWriter.write("screenRes, " + Toolkit.getDefaultToolkit().getScreenResolution() + "\n");
            //System.out.println("scrren res is " + Toolkit.getDefaultToolkit().getScreenResolution());
            fileWriter.write("Class, " + task.getType() + "\n");
            fileWriter.write("End of Parameters \n");
            fileWriter.write(firstline + task.getExtraDataLabel() + "\n");
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    private void getCalibration(String outputFileName, double screenDistance) {
        CalibrationResult result = gm.getLastCalibrationResult();
        CalibrationResult.CalibrationPoint[] points = result.calibpoints;
        File file = new File(outputFileName);
        int star = 1;
        // Calculating star rating based on average error as seen in:
        // http://theeyetribe.com/forum/viewtopic.php?f=9&t=332&p=1382#p1382
        if (result.averageErrorDegree < 0.5) {
            star = 5;
        } else if (result.averageErrorDegree < 0.7) {
            star = 4;
        } else if (result.averageErrorDegree < 1) {
            star = 3;
        } else if (result.averageErrorDegree < 1.4) {
            star = 2;
        }

        try {
            file.createNewFile();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            calibrationWriter = new FileWriter(file);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            calibrationWriter.write("Class, CalibrationData\n");
            calibrationWriter.write("Screen Dist (In), " + Double.toString(screenDistance) + "\n");
            calibrationWriter.write("screenRes, " + Toolkit.getDefaultToolkit().getScreenResolution() + "\n");
            calibrationWriter.write("Stars, " + Integer.toString(star) + "\n");
            calibrationWriter.write("Average Error Degree, " + Double.toString(result.averageErrorDegree) + "\n");
            calibrationWriter.write("Average Error Degree Left, " + Double.toString(result.averageErrorDegreeLeft) + "\n");
            calibrationWriter.write("Average Error Degree Right, " + Double.toString(result.averageErrorDegreeRight) + "\n");
            for (int i = 0; i < points.length; i++) {
                calibrationWriter.write("accuracyDegreesMean" + Integer.toString(i) + ", " + Double.toString(points[i].accuracy.accuracyDegrees) + "\n");
                calibrationWriter.write("accuracyDegreesLeft" + Integer.toString(i) + ", " + Double.toString(points[i].accuracy.accuracyDegreesLeft) + "\n");
                calibrationWriter.write("accuracyDegreesRight" + Integer.toString(i) + ", " + Double.toString(points[i].accuracy.accuracyDegreesRight) + "\n");
                calibrationWriter.write("ErrorPixelsMean" + Integer.toString(i) + ", " + Double.toString(points[i].meanError.meanErrorPixels) + "\n");
                calibrationWriter.write("ErrorPixelsLeft" + Integer.toString(i) + ", " + Double.toString(points[i].meanError.meanErrorPixelsLeft) + "\n");
                calibrationWriter.write("ErrorPixelsRight" + Integer.toString(i) + ", " + Double.toString(points[i].meanError.meanErrorPixelsRight) + "\n");
                calibrationWriter.write("StdDevPixelsMean" + Integer.toString(i) + ", " + Double.toString(points[i].standardDeviation.averageStandardDeviationPixels) + "\n");
                calibrationWriter.write("StdDevPixelsLeft" + Integer.toString(i) + ", " + Double.toString(points[i].standardDeviation.averageStandardDeviationPixelsLeft) + "\n");
                calibrationWriter.write("StdDevPixelsRight" + Integer.toString(i) + ", " + Double.toString(points[i].standardDeviation.averageStandardDeviationPixelsRight) + "\n");
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            calibrationWriter.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            calibrationWriter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void updateGazeData(String dataStr) {
        gazeData.add(dataStr);
    }

    public void writeTestNote(String testNote) {
        try {
            fileWriter.write(testNote + "\n");
            System.out.println(testNote + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void finishWriter() {
        try {
            for (String dataStr : gazeData) {
                fileWriter.write(dataStr + "\n");
            }
            //System.out.println(dataStr + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileWriter.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            fileWriter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
