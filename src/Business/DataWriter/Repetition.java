/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DataWriter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author neethan
 */
public class Repetition {
    private HashMap<String, ArrayList<Integer>> params;

    public Repetition() {
        params = new HashMap<String, ArrayList<Integer>>();
        params.put("object1X", new ArrayList<Integer>());
        params.put("object1Y", new ArrayList<Integer>());
        params.put("object2X", new ArrayList<Integer>());
        params.put("object2Y", new ArrayList<Integer>());
        params.put("rawX", new ArrayList<Integer>());
        params.put("rawY", new ArrayList<Integer>());
        params.put("leftX", new ArrayList<Integer>());
        params.put("leftY", new ArrayList<Integer>());
        params.put("rightX", new ArrayList<Integer>());
        params.put("rightY", new ArrayList<Integer>());
        params.put("timeDelay", new ArrayList<Integer>());
        params.put("indicator1", new ArrayList<Integer>());
        params.put("indicator2", new ArrayList<Integer>());
    }

    public HashMap<String, ArrayList<Integer>> getParams() {
        return params;
    }

    public void setParams(HashMap<String, ArrayList<Integer>> params) {
        this.params = params;
    }
}
