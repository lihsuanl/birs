/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Validation;

/**
 *
 * @author Neethan
 */
public class Validation {

    public static boolean stringIsEmpty(String s) {
        if (s.trim().equals("") || s == null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean allChar(String s) {
        if (s.matches("[a-zA-Z]+")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 != 0) {
            return false;
        } else if (year % 100 != 0) {
            return true;
        } else {
            return year % 400 == 0;
        }
    }

    public static int dayOfMonth(int year, int month) {
        if (month == 2) {
            if (isLeapYear(year)) {
                return 29;
            }
            return 28;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        } else {
            return 31;
        }
    }
}
