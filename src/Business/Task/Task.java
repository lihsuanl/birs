/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Task;

import Business.Business.Business;
import Business.Patient.Patient;
import com.theeyetribe.client.GazeManager;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Hashtable;
import javax.swing.JPanel;

/**
 *
 * @author neethan
 */
public abstract class Task {

    private String type;
    private String instruction;
    private double screenDistance;
    private int numOfTarget;
    private Hashtable taskParams;
    private String extraDataLabel;
    private String resultTitle;

    public Task(String type) {
        this.type = type;
        screenDistance = 20;
    }

    public Task() {
    }

    public abstract JPanel createNextTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm);

    public abstract JPanel createTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setResultTitle(String resultTitle) {
        this.resultTitle = resultTitle;
    }

    public String getResultTitle() {
        return resultTitle;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public double getScreenDistance() {
        return screenDistance;
    }

    public void setScreenDistance(double screenDistance) {
        this.screenDistance = screenDistance;
    }

    public int getNumOfTarget() {
        return numOfTarget;
    }

    public void setNumOfTarget(int numOfTarget) {
        this.numOfTarget = numOfTarget;
    }

    public Hashtable getTaskParams() {
        return taskParams;
    }

    public void setTaskParams(Hashtable taskParams) {
        this.taskParams = taskParams;
    }

    public String getExtraDataLabel() {
        return extraDataLabel;
    }

    public void setExtraDataLabel(String extraDataLabel) {
        this.extraDataLabel = extraDataLabel;
    }

    public void getResolution() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dim = toolkit.getScreenSize();
        //System.out.println("Width of Screen Size is " + dim.width + " pixels");
        //System.out.println("Height of Screen Size is " + dim.height + " pixels");
        int resolution = Toolkit.getDefaultToolkit().getScreenResolution();
        //System.out.println(resolution);
    }

    public int getPixels(double inch) {
        return (int) (inch * 216);
    }

    @Override
    public String toString() {
        return this.type;
    }

}
