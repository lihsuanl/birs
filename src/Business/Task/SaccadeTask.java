/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Task;

import Business.Business.Business;
import Business.Patient.Patient;
import UserInterface.AntiSaccadeTest.AntiSaccadeTestJPanel;
import UserInterface.SaccadeTest.SaccadeTestJPanel;
import com.theeyetribe.client.GazeManager;
import java.util.Hashtable;
import javax.swing.JPanel;

/**
 *
 * @author neethan
 */
public class SaccadeTask extends Task {

    private boolean isDone;
    private Hashtable<String, Double> taskParams;

    public SaccadeTask() {
        super("SaccadeTask");
        isDone = false;
        this.setNumOfTarget(1);
        initiateParamTable();
    }

    private void initiateParamTable() {
        taskParams = new Hashtable();
        taskParams.put("Focus Time Low", 1000.0);
        taskParams.put("Focus Time High", 3000.0);
        taskParams.put("Presentation Per Side", 10.0);
        taskParams.put("Pulse Time", 2000.0);
        taskParams.put("Saccade-0; Anti-1", 0.0);
        taskParams.put("Target Offset (Deg)", 15.0);
        taskParams.put("Pulses", 3.0);
        taskParams.put("Pulse Width Max", 20.0);
        taskParams.put("Screen Distance (In)", 20.0);
        this.setTaskParams(taskParams);
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public JPanel createNextTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        // TODO initiate a new Test JPanel for next Task
        return new AntiSaccadeTestJPanel(userProcessContainer, business, patient, gm);
    }

    @Override
    public JPanel createTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        return new SaccadeTestJPanel(userProcessContainer, business, patient, gm);//To change body of generated methods, choose Tools | Templates.
    }

}
