/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Task;

import Business.Business.Business;
import Business.Patient.Patient;
import UserInterface.DistanceVergenceTest.DistanceVergenceTestJPanel;
import UserInterface.SaccadeTest.SaccadeTestJPanel;
import com.theeyetribe.client.GazeManager;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Hashtable;
import javax.swing.JPanel;

/**
 *
 * @author neethan
 */
public class DistanceVergenceTask extends Task {

    private boolean isDone;
    private int rep;
    private Color bg1, bg2;
    private double minTime, maxTime, dist1, dist2;
    private double targetRadius;

    public DistanceVergenceTask() {
        super("DistanceVergenceTask");
        isDone = false;
        rep = 30;
        this.setNumOfTarget(2);
        this.setExtraDataLabel(", focusDistance");
        initiateAnimationParams();
        initiateParamTable();
    }

    private void initiateAnimationParams() {
        this.bg1 = Color.BLACK;
        this.bg2 = Color.WHITE;
        this.minTime = 2000;
        this.maxTime = 3000;
        this.dist1 = 6.25;
        this.dist2 = 20;
    }

    private void initiateParamTable() {
        Hashtable<String, Double> taskParams = new Hashtable();
        taskParams.put("Reps", 10.0);
        taskParams.put("Screen Dist (In)", 20.0);
        taskParams.put("Focus Distance 1:(in)", 4.0);
        taskParams.put("Focus Distance 2:(in)", 20.0);
        taskParams.put("Min Time(ms)", 2000.0);
        taskParams.put("Max Time ", 3000.0);
        this.setTaskParams(taskParams);
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public int getRep() {
        return rep;
    }

    public void setRep(int rep) {
        this.rep = rep;
    }

    public Color getBg1() {
        return bg1;
    }

    public void setBg1(Color bg1) {
        this.bg1 = bg1;
    }

    public Color getBg2() {
        return bg2;
    }

    public void setBg2(Color bg2) {
        this.bg2 = bg2;
    }

    public double getMinTime() {
        return minTime;
    }

    public void setMinTime(double minTime) {
        this.minTime = minTime;
    }

    public double getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(double maxTime) {
        this.maxTime = maxTime;
    }

    public double getDist1() {
        return dist1;
    }

    public void setDist1(double dist1) {
        this.dist1 = dist1;
    }

    public double getDist2() {
        return dist2;
    }

    public void setDist2(double dist2) {
        this.dist2 = dist2;
    }

    public double getTargetRadius() {
        return targetRadius;
    }

    public void setTargetRadius(double targetRadius) {
        this.targetRadius = targetRadius;
    }

    @Override
    public JPanel createNextTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        // TODO initiate a new Test JPanel for next Task
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return new SaccadeTestJPanel(userProcessContainer, business, patient, gm);
    }

    @Override
    public JPanel createTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        return new DistanceVergenceTestJPanel(userProcessContainer, business, patient, gm); //To change body of generated methods, choose Tools | Templates.
    }

}
