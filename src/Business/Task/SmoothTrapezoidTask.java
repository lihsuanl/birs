/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Task;

import Business.Business.Business;
import Business.Patient.Patient;
import UserInterface.DistanceVergenceTest.DistanceVergenceTestJPanel;
import UserInterface.TrapezoidTest.TrapezoidTestJPanel;
import com.theeyetribe.client.GazeManager;
import java.util.Hashtable;
import javax.swing.JPanel;

/**
 *
 * @author neethan
 */
public class SmoothTrapezoidTask extends Task {

    private int[] cycleTimeSet;
    private int noOfCycle;
    private boolean isDone;

    public SmoothTrapezoidTask() {
        super("SmoothTrapezoidTask");
        int[] a = {2400, 1600, 1200, 800};
        setCycleParams(a, 10);
        isDone = false;
        this.setNumOfTarget(1);
        this.setExtraDataLabel(", Current Round, Current CycleTimeIndex");
        initiateParamTable();
    }

    private void initiateParamTable() {
        Hashtable<String, Double> taskParams = new Hashtable();
        taskParams.put("Refresh Rate (ms)", 20.0);
        taskParams.put("Screen Dist (In)", 20.0);
        taskParams.put("Initial Speed (Deg/s)", 20.0);
        taskParams.put("Final Speed (Deg/s)", 120.0);
        taskParams.put("N Speeds (Deg/s^2)", 6.0);
        taskParams.put("Cycles per Speed (ms)", 6.0);
        taskParams.put("Path Radius (Deg)", 15.0);
        taskParams.put("Radius (Deg)", 0.5);
        taskParams.put("Pause Time (s)", 0.5);
        this.setTaskParams(taskParams);
    }

    public void setCycleParams(int[] cyclesTimes, int noOfCycles) {
        this.cycleTimeSet = cyclesTimes;
        this.noOfCycle = noOfCycles;
    }

    public int[] getCycleTimeSet() {
        return cycleTimeSet;
    }

    public void setCycleTimeSet(int[] cycleTimeSet) {
        this.cycleTimeSet = cycleTimeSet;
    }

    public int getNoOfCycle() {
        return noOfCycle;
    }

    public void setNoOfCycle(int noOfCycle) {
        this.noOfCycle = noOfCycle;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public JPanel createNextTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        // TODO initiate a new Test JPanel for next Task
        return new DistanceVergenceTestJPanel(userProcessContainer, business, patient, gm);
    }

    @Override
    public JPanel createTestJPanel(JPanel userProcessContainer, Business business, Patient patient, GazeManager gm) {
        return new TrapezoidTestJPanel(userProcessContainer, business, patient, gm);//To change body of generated methods, choose Tools | Templates.
    }

}
