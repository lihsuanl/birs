/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import java.util.ArrayList;

/**
 *
 * @author neethan
 */
public class PatientDirectory {

    private ArrayList<Patient> patientList;

    public PatientDirectory() {
        patientList = new ArrayList<>();
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<Patient> patientList) {
        this.patientList = patientList;
    }

    // The function is used to create a new patient and add it to the list and return the new object
    public Patient createAndAddPatient() {
        Patient patient = new Patient();
        patientList.add(patient);
        return patient;
    }

    // The function is used to add an existed patient into the list
    public void addPatient(Patient patient) {
        patientList.add(patient);
    }

    // The function is used to remove a patient from the list
    public void removePatient(Patient patient) {
        patientList.remove(patient);
    }

    // The function is used to find if the patient is existed
    public boolean isPatientExisted(String patientId) {
        for (Patient p : patientList) {
            if (p.getPatientId().equals(patientId)) {
                return true;
            }
        }
        return false;
    }

    // The function is used to get the existed patient out for further use
    public Patient authenticatePatient(String patientId, String name) {
        for (Patient p : patientList) {
            if (p.getPatientId().equals(patientId) && p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }
}
