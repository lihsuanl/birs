/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import Business.Test.TestHistory;

/**
 *
 * @author neethan
 */
public class Patient {

    private String patientId;
    private String name;
    private String dateOfBirth;
    private String gender;
    private static int counter = 0;
    private String outputFolder;
    private TestHistory testHistory;

    /**
     * The following are two Constructors for this class. Can either create a
     * patient with all the info like name, gender and date of birth or create a
     * patient without any information. The patient ID is supposed to be set
     * automatically, add 1 each time start from 0.
     */
    public Patient() {
        counter++;
        testHistory = new TestHistory();
        outputFolder = "";
    }

    public Patient(String patientId) {
        counter++;
        this.patientId = patientId;
        testHistory = new TestHistory();
        outputFolder = "";
    }

    public Patient(String patientId, String name, String dateOfBirth, String gender) {
        counter++;
        this.patientId = patientId;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        testHistory = new TestHistory();
        outputFolder = "";
    }

    // The following are Getter and Setter of all atributes of this class
    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public TestHistory getTestHistory() {
        return testHistory;
    }

    public void setTestHistory(TestHistory testHistory) {
        this.testHistory = testHistory;
    }

    // The function override the toString method, which will return the name of the patient
    @Override
    public String toString() {
        return name;
    }
}
